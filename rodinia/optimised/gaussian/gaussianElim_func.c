

void gaussian1(float *m_dev, float *a_dev, float *b_dev, const int size, const int t, int globalId)
{
    if (globalId < size-1-t) {
         *(m_dev + size * (globalId + t + 1)+t) = *(a_dev + size * (globalId + t + 1) + t) / *(a_dev + size * t + t);    
    }
}

void gaussian2(float *m_dev, float *a_dev, float *b_dev, const int size, const int t, int globalId, int globalIdx, int globalIdy)
{
	if (globalIdx < size-1-t && globalIdy < size-t)
	{
		a_dev[size*(globalIdx+1+t)+(globalIdy+t)] -= m_dev[size*(globalIdx+1+t)+t] * a_dev[size*t+(globalIdy+t)];

 	    if(globalIdy == 0)
		{
			b_dev[globalIdx+1+t] -= m_dev[size*(globalIdx+1+t)+(globalIdy+t)] * b_dev[t];
 	    }
	}
}
