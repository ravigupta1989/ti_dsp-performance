//#pragma OPENCL EXTENSION cl_khr_byte_addressable_store : enable

typedef struct latLong
    {
        float lat;
        float lng;
    } LatLong;

__kernel void Fan1(__global float *m_dev, __global float *a_dev, __global float *b_dev, const int size, const int t)
{
    int globalId = get_global_id(0);

	gaussian1(*m_dev, *a_dev, *b_dev, size, t, globalId);
}


__kernel void Fan2(__global float *m_dev, __global float *a_dev, __global float *b_dev, const int size, const int t)
{
	int globalId = get_global_id(0);
	int globalIdx = get_global_id(0);
	int globalIdy = get_global_id(1);

	gaussian2(*m_dev, *a_dev, *b_dev, size, t, globalId, globalIdx, globalIdy);
}
