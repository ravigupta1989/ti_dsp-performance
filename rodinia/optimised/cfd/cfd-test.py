import subprocess
import sys
import os
import time

# Define these values:   ~~~~~~~~~~~~~~~~~~~~~~~~

#input_data = ['missile.domn.0.2M -t acc -d 0']
input_data = ['fvcorr.domn.097K -t acc -d 0']
num_iterations = 1;

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

if len(sys.argv) != 3 :
	print("Usage: python2 cfd-test.py <output_filename> <list of wgsizes>\n\n")
	sys.exit()

size_list = sys.argv[2].split(",")

f = open(sys.argv[1], 'w')

for wgsize in size_list:
	#compile_str = "make KERNEL_DIM=\"-DRD_WG_SIZE_1=" + size + " -DRD_WG_SIZE_2=" + size + " -DRD_WG_SIZE_3=" + size + " -DRD_WG_SIZE_4=" + size + "\""
	compile_str = "make"
	os.system("make clean")
	os.system(compile_str)
	time.sleep(1)
	
	for data in input_data :
		f.write("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n")
		f.write("Input: " + data + "\n")
		f.write("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n")

		for i in range(1, num_iterations+1):
			result =  subprocess.check_output(['./euler3d', data])
			f.write("ABCD : WGSIZE : " + wgsize + "\n")
			f.write("ABCD : INPUT : " + data + "\n")
			f.write("ABCD : NUM_I : " + str(num_iterations) + "\n")
			f.write("ABCD : ITERATION : " + str(i) + "\n")
			
			f.write(result)

			f.write("ABCD : END\n")
			f.write("\n\n")













