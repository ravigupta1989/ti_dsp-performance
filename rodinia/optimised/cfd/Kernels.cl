/* ============================================================
//--functions: 	kernel funtion
//--programmer:	Jianbin Fang
//--date:		24/03/2011
============================================================ */
#ifndef _KERNEL_
#define _KERNEL_

#define GAMMA (1.4f)


#define NDIM 3
#define NNB 4

#define RK 3	// 3rd order RK
#define ff_mach 1.2f
#define deg_angle_of_attack 0.0f

#define VAR_DENSITY 0
#define VAR_MOMENTUM  1
#define VAR_DENSITY_ENERGY (VAR_MOMENTUM+NDIM)
#define NVAR (VAR_DENSITY_ENERGY+1)
//#pragma OPENCL EXTENSION CL_MAD : enable

//self-defined user type
typedef struct{
	float x;
	float y;
	float z;
} FLOAT3;



__kernel void memset_kernel(__global char * mem_d, short val, int ct)
{
	const int thread_id = get_global_id(0);
	kernel1(mem_d, val, ct, thread_id);
}

__kernel void initialize_variables(__global float* variables, __constant float* ff_variable, int nelr)
{
	const int i = get_global_id(0);
	kernel2(variables, ff_variable, nelr, i);
}

__kernel void compute_step_factor(__global float* variables, __global float* areas, __global float* step_factors, int nelr)
{
	const int i = get_global_id(0);	
	kernel3(variables, areas, step_factors, nelr, i);
}

__kernel void compute_flux(
					__global int* elements_surrounding_elements, 
					__global float* normals, 
					__global float* variables, 
					__constant float* ff_variable,
					__global float* fluxes,
					__constant FLOAT3* ff_flux_contribution_density_energy,
					__constant FLOAT3* ff_flux_contribution_momentum_x,
					__constant FLOAT3* ff_flux_contribution_momentum_y,
					__constant FLOAT3* ff_flux_contribution_momentum_z,
					int nelr)
{
	const int i = get_global_id(0);
	kernel4(elements_surrounding_elements, normals, variables, ff_variable, fluxes, ff_flux_contribution_density_energy, ff_flux_contribution_momentum_x, ff_flux_contribution_momentum_y, ff_flux_contribution_momentum_z, nelr, i);

}

__kernel void time_step(int j, int nelr, 
				__global float* old_variables, 
				__global float* variables, 
				__global float* step_factors, 
				__global float* fluxes)
{
	const int i = get_global_id(0);
	kernel5(j, nelr, old_variables, variables, step_factors, fluxes, i);
}

#endif
