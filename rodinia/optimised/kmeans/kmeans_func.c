#ifndef FLT_MAX
#define FLT_MAX 3.40282347e+38
#endif

void kmeans1(float * feature, float * clusters, int * membership, int npoints, int nclusters, int nfeatures, int offset, int size, unsigned int point_id);
void kmeans2(float * feature, float * feature_swap, int npoints, int nfeatures, unsigned int tid);

void kmeans1(float * feature, float * clusters, int * membership, int npoints, int nclusters, int nfeatures, int offset, int size, unsigned int point_id) 
{
    int index = 0;
	int i = 0;
	int l = 0;
    //const unsigned int point_id = get_global_id(0);
		if (point_id < npoints)
		{
			float min_dist=FLT_MAX;
			for (i=0; i < nclusters; i++) {
				
				float dist = 0;
				float ans  = 0;
				for (l=0; l<nfeatures; l++){
						ans += (feature[l * npoints + point_id]-clusters[i*nfeatures+l])* 
							   (feature[l * npoints + point_id]-clusters[i*nfeatures+l]);
				}

				dist = ans;
				if (dist < min_dist) {
					min_dist = dist;
					index    = i;
				}
			}
		  //printf("%d\n", index);
		  membership[point_id] = index;
		}
		
	return;
}

void kmeans2(float * feature, float * feature_swap, int npoints, int nfeatures, unsigned int tid)
{
	int i = 0;
	
	for(i = 0; i <  nfeatures; i++)
		feature_swap[i * npoints + tid] = feature[tid * nfeatures + i];
}




