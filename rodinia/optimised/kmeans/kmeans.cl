#ifndef FLT_MAX
#define FLT_MAX 3.40282347e+38
#endif

//#include "kmeans_func.c"

__kernel void kmeans_kernel_c(__global float  *feature,   
			  __global float  *clusters,
			  __global int    *membership,
			    int     npoints,
				int     nclusters,
				int     nfeatures,
				int		offset,
				int		size
			  ) 
{

	unsigned int point_id = get_global_id(0);
	
	kmeans1(*feature, *clusters, *membership, npoints, nclusters, nfeatures, offset, size, point_id);
	
	//printf("Finished K1\n");
	
	return;
}

__kernel void
kmeans_swap(__global float  *feature,   
			__global float  *feature_swap,
			int     npoints,
			int     nfeatures
){
 
	unsigned int tid = get_global_id(0);
	
	kmeans2(*feature, *feature_swap, npoints, nfeatures, tid);
	
	//printf("Finished K2\n");
	
	return;
} 
