import subprocess
import sys
import os
import time

# Define these values:   ~~~~~~~~~~~~~~~~~~~~~~~~

input_data = ['./pathfinder 10000 100 20', './pathfinder 50000 100 20', './pathfinder 100000 100 20']
num_iterations = 5;

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

if len(sys.argv) != 2 :
	print("Usage: python2 pathfinder-test.py <output_filename> <list of wgsizes>\n\n")
	sys.exit()

#size_list = sys.argv[2].split(",")

f = open(sys.argv[1], 'w')


#for wgsize in size_list:
	
#compile_str = "make KERNEL_DIM=\"-DRD_WG_SIZE_0=" + wgsize + "\""

compile_str = "make"
os.system("make clean")
os.system(compile_str)
time.sleep(1)

for data in input_data:
	f.write("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n")
	f.write("Input: " + data + "\n")
	f.write("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n")

	for i in range(1, num_iterations+1):
		result =  subprocess.check_output(data.split(' '))

		#f.write("ABCD : WGSIZE : " + wgsize + "\n")
		f.write("ABCD : INPUT : " + data + "\n")
		f.write("ABCD : NUM_I : " + str(num_iterations) + "\n")
		f.write("ABCD : ITERATION : " + str(i) + "\n")
		time.sleep(1)
		f.write(result)
		f.write("ABCD : END")
		f.write("\n\n")












