~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Input: graph262144.txt
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

ABCD : WGSIZE : 2048
ABCD : INPUT : graph262144.txt
ABCD : NUM_I : 3
ABCD : ITERATION : 1
Reading File
no_of_nodes 262144 num_of_blocks 1 work_group_size 2048
Starting bfs on dsp...
Creating Buffers...
Buffers Created.
Setting values of buffers...
Done setting values.
Creating Kernels...
Kernels created.
Running Kernels...
ABCD : BFS : K1 : 23020 microseconds
ABCD : BFS : K2 : 725 microseconds
ABCD : BFS : K1 : 1353 microseconds
ABCD : BFS : K2 : 724 microseconds
ABCD : BFS : K1 : 1374 microseconds
ABCD : BFS : K2 : 875 microseconds
ABCD : BFS : K1 : 1598 microseconds
ABCD : BFS : K2 : 1563 microseconds
ABCD : BFS : K1 : 2617 microseconds
ABCD : BFS : K2 : 4887 microseconds
ABCD : BFS : K1 : 8171 microseconds
ABCD : BFS : K2 : 18418 microseconds
ABCD : BFS : K1 : 30839 microseconds
ABCD : BFS : K2 : 62575 microseconds
ABCD : BFS : K1 : 91700 microseconds
ABCD : BFS : K2 : 92204 microseconds
ABCD : BFS : K1 : 110446 microseconds
ABCD : BFS : K2 : 18683 microseconds
ABCD : BFS : K1 : 21013 microseconds
ABCD : BFS : K2 : 755 microseconds
ABCD : BFS : K1 : 1425 microseconds
ABCD : BFS : K2 : 694 microseconds
Completed Kernels. #iterations=11
Done!
Finished running bfs on dsp.
Printing results...
Result stored in result.txt
ABCD : END


ABCD : WGSIZE : 2048
ABCD : INPUT : graph262144.txt
ABCD : NUM_I : 3
ABCD : ITERATION : 2
Reading File
no_of_nodes 262144 num_of_blocks 1 work_group_size 2048
Starting bfs on dsp...
Creating Buffers...
Buffers Created.
Setting values of buffers...
Done setting values.
Creating Kernels...
Kernels created.
Running Kernels...
ABCD : BFS : K1 : 20986 microseconds
ABCD : BFS : K2 : 742 microseconds
ABCD : BFS : K1 : 1388 microseconds
ABCD : BFS : K2 : 742 microseconds
ABCD : BFS : K1 : 1407 microseconds
ABCD : BFS : K2 : 874 microseconds
ABCD : BFS : K1 : 1580 microseconds
ABCD : BFS : K2 : 1584 microseconds
ABCD : BFS : K1 : 2638 microseconds
ABCD : BFS : K2 : 4902 microseconds
ABCD : BFS : K1 : 8280 microseconds
ABCD : BFS : K2 : 18441 microseconds
ABCD : BFS : K1 : 30779 microseconds
ABCD : BFS : K2 : 62531 microseconds
ABCD : BFS : K1 : 91610 microseconds
ABCD : BFS : K2 : 92189 microseconds
ABCD : BFS : K1 : 110451 microseconds
ABCD : BFS : K2 : 18688 microseconds
ABCD : BFS : K1 : 21017 microseconds
ABCD : BFS : K2 : 758 microseconds
ABCD : BFS : K1 : 1416 microseconds
ABCD : BFS : K2 : 695 microseconds
Completed Kernels. #iterations=11
Done!
Finished running bfs on dsp.
Printing results...
Result stored in result.txt
ABCD : END


ABCD : WGSIZE : 2048
ABCD : INPUT : graph262144.txt
ABCD : NUM_I : 3
ABCD : ITERATION : 3
Reading File
no_of_nodes 262144 num_of_blocks 1 work_group_size 2048
Starting bfs on dsp...
Creating Buffers...
Buffers Created.
Setting values of buffers...
Done setting values.
Creating Kernels...
Kernels created.
Running Kernels...
ABCD : BFS : K1 : 23489 microseconds
ABCD : BFS : K2 : 724 microseconds
ABCD : BFS : K1 : 1354 microseconds
ABCD : BFS : K2 : 722 microseconds
ABCD : BFS : K1 : 1391 microseconds
ABCD : BFS : K2 : 871 microseconds
ABCD : BFS : K1 : 1583 microseconds
ABCD : BFS : K2 : 1579 microseconds
ABCD : BFS : K1 : 2637 microseconds
ABCD : BFS : K2 : 4906 microseconds
ABCD : BFS : K1 : 8181 microseconds
ABCD : BFS : K2 : 18426 microseconds
ABCD : BFS : K1 : 30834 microseconds
ABCD : BFS : K2 : 62576 microseconds
ABCD : BFS : K1 : 91616 microseconds
ABCD : BFS : K2 : 92183 microseconds
ABCD : BFS : K1 : 110381 microseconds
ABCD : BFS : K2 : 18692 microseconds
ABCD : BFS : K1 : 21029 microseconds
ABCD : BFS : K2 : 761 microseconds
ABCD : BFS : K1 : 1434 microseconds
ABCD : BFS : K2 : 689 microseconds
Completed Kernels. #iterations=11
Done!
Finished running bfs on dsp.
Printing results...
Result stored in result.txt
ABCD : END


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Input: graph262144.txt
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

ABCD : WGSIZE : 4096
ABCD : INPUT : graph262144.txt
ABCD : NUM_I : 3
ABCD : ITERATION : 1
Reading File
no_of_nodes 262144 num_of_blocks 1 work_group_size 4096
Starting bfs on dsp...
Creating Buffers...
Buffers Created.
Setting values of buffers...
Done setting values.
Creating Kernels...
Kernels created.
Running Kernels...
ABCD : BFS : K1 : 22794 microseconds
ABCD : BFS : K2 : 716 microseconds
ABCD : BFS : K1 : 1418 microseconds
ABCD : BFS : K2 : 724 microseconds
ABCD : BFS : K1 : 1432 microseconds
ABCD : BFS : K2 : 839 microseconds
ABCD : BFS : K1 : 1619 microseconds
ABCD : BFS : K2 : 1553 microseconds
ABCD : BFS : K1 : 2604 microseconds
ABCD : BFS : K2 : 4959 microseconds
ABCD : BFS : K1 : 8162 microseconds
ABCD : BFS : K2 : 18462 microseconds
ABCD : BFS : K1 : 30939 microseconds
ABCD : BFS : K2 : 62201 microseconds
ABCD : BFS : K1 : 91800 microseconds
ABCD : BFS : K2 : 90969 microseconds
ABCD : BFS : K1 : 109478 microseconds
ABCD : BFS : K2 : 18689 microseconds
ABCD : BFS : K1 : 21101 microseconds
ABCD : BFS : K2 : 754 microseconds
ABCD : BFS : K1 : 1461 microseconds
ABCD : BFS : K2 : 677 microseconds
Completed Kernels. #iterations=11
Done!
Finished running bfs on dsp.
Printing results...
Result stored in result.txt
ABCD : END


ABCD : WGSIZE : 4096
ABCD : INPUT : graph262144.txt
ABCD : NUM_I : 3
ABCD : ITERATION : 2
Reading File
no_of_nodes 262144 num_of_blocks 1 work_group_size 4096
Starting bfs on dsp...
Creating Buffers...
Buffers Created.
Setting values of buffers...
Done setting values.
Creating Kernels...
Kernels created.
Running Kernels...
ABCD : BFS : K1 : 23770 microseconds
ABCD : BFS : K2 : 716 microseconds
ABCD : BFS : K1 : 1401 microseconds
ABCD : BFS : K2 : 727 microseconds
ABCD : BFS : K1 : 1451 microseconds
ABCD : BFS : K2 : 859 microseconds
ABCD : BFS : K1 : 1630 microseconds
ABCD : BFS : K2 : 1555 microseconds
ABCD : BFS : K1 : 2625 microseconds
ABCD : BFS : K2 : 4951 microseconds
ABCD : BFS : K1 : 8188 microseconds
ABCD : BFS : K2 : 18494 microseconds
ABCD : BFS : K1 : 30904 microseconds
ABCD : BFS : K2 : 62227 microseconds
ABCD : BFS : K1 : 91849 microseconds
ABCD : BFS : K2 : 90985 microseconds
ABCD : BFS : K1 : 109441 microseconds
ABCD : BFS : K2 : 18672 microseconds
ABCD : BFS : K1 : 21145 microseconds
ABCD : BFS : K2 : 757 microseconds
ABCD : BFS : K1 : 1457 microseconds
ABCD : BFS : K2 : 677 microseconds
Completed Kernels. #iterations=11
Done!
Finished running bfs on dsp.
Printing results...
Result stored in result.txt
ABCD : END


ABCD : WGSIZE : 4096
ABCD : INPUT : graph262144.txt
ABCD : NUM_I : 3
ABCD : ITERATION : 3
Reading File
no_of_nodes 262144 num_of_blocks 1 work_group_size 4096
Starting bfs on dsp...
Creating Buffers...
Buffers Created.
Setting values of buffers...
Done setting values.
Creating Kernels...
Kernels created.
Running Kernels...
ABCD : BFS : K1 : 23686 microseconds
ABCD : BFS : K2 : 716 microseconds
ABCD : BFS : K1 : 1416 microseconds
ABCD : BFS : K2 : 725 microseconds
ABCD : BFS : K1 : 1451 microseconds
ABCD : BFS : K2 : 838 microseconds
ABCD : BFS : K1 : 1627 microseconds
ABCD : BFS : K2 : 1553 microseconds
ABCD : BFS : K1 : 2609 microseconds
ABCD : BFS : K2 : 4945 microseconds
ABCD : BFS : K1 : 8162 microseconds
ABCD : BFS : K2 : 18480 microseconds
ABCD : BFS : K1 : 30880 microseconds
ABCD : BFS : K2 : 62202 microseconds
ABCD : BFS : K1 : 91613 microseconds
ABCD : BFS : K2 : 90951 microseconds
ABCD : BFS : K1 : 109435 microseconds
ABCD : BFS : K2 : 18672 microseconds
ABCD : BFS : K1 : 21093 microseconds
ABCD : BFS : K2 : 757 microseconds
ABCD : BFS : K1 : 1473 microseconds
ABCD : BFS : K2 : 676 microseconds
Completed Kernels. #iterations=11
Done!
Finished running bfs on dsp.
Printing results...
Result stored in result.txt
ABCD : END


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Input: graph262144.txt
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

ABCD : WGSIZE : 8192
ABCD : INPUT : graph262144.txt
ABCD : NUM_I : 3
ABCD : ITERATION : 1
Reading File
no_of_nodes 262144 num_of_blocks 1 work_group_size 8192
Starting bfs on dsp...
Creating Buffers...
Buffers Created.
Setting values of buffers...
Done setting values.
Creating Kernels...
Kernels created.
Running Kernels...
ABCD : BFS : K1 : 24109 microseconds
ABCD : BFS : K2 : 680 microseconds
ABCD : BFS : K1 : 1537 microseconds
ABCD : BFS : K2 : 684 microseconds
ABCD : BFS : K1 : 1546 microseconds
ABCD : BFS : K2 : 826 microseconds
ABCD : BFS : K1 : 1821 microseconds
ABCD : BFS : K2 : 1622 microseconds
ABCD : BFS : K1 : 2876 microseconds
ABCD : BFS : K2 : 4831 microseconds
ABCD : BFS : K1 : 8090 microseconds
ABCD : BFS : K2 : 18650 microseconds
ABCD : BFS : K1 : 31323 microseconds
ABCD : BFS : K2 : 62360 microseconds
ABCD : BFS : K1 : 91690 microseconds
ABCD : BFS : K2 : 91097 microseconds
ABCD : BFS : K1 : 110392 microseconds
ABCD : BFS : K2 : 18800 microseconds
ABCD : BFS : K1 : 21422 microseconds
ABCD : BFS : K2 : 734 microseconds
ABCD : BFS : K1 : 1563 microseconds
ABCD : BFS : K2 : 651 microseconds
Completed Kernels. #iterations=11
Done!
Finished running bfs on dsp.
Printing results...
Result stored in result.txt
ABCD : END


ABCD : WGSIZE : 8192
ABCD : INPUT : graph262144.txt
ABCD : NUM_I : 3
ABCD : ITERATION : 2
Reading File
no_of_nodes 262144 num_of_blocks 1 work_group_size 8192
Starting bfs on dsp...
Creating Buffers...
Buffers Created.
Setting values of buffers...
Done setting values.
Creating Kernels...
Kernels created.
Running Kernels...
ABCD : BFS : K1 : 23198 microseconds
ABCD : BFS : K2 : 682 microseconds
ABCD : BFS : K1 : 1551 microseconds
ABCD : BFS : K2 : 720 microseconds
ABCD : BFS : K1 : 1531 microseconds
ABCD : BFS : K2 : 824 microseconds
ABCD : BFS : K1 : 1845 microseconds
ABCD : BFS : K2 : 1628 microseconds
ABCD : BFS : K1 : 2921 microseconds
ABCD : BFS : K2 : 4827 microseconds
ABCD : BFS : K1 : 8073 microseconds
ABCD : BFS : K2 : 18668 microseconds
ABCD : BFS : K1 : 31276 microseconds
ABCD : BFS : K2 : 62392 microseconds
ABCD : BFS : K1 : 91780 microseconds
ABCD : BFS : K2 : 91029 microseconds
ABCD : BFS : K1 : 110310 microseconds
ABCD : BFS : K2 : 18783 microseconds
ABCD : BFS : K1 : 21418 microseconds
ABCD : BFS : K2 : 735 microseconds
ABCD : BFS : K1 : 1575 microseconds
ABCD : BFS : K2 : 650 microseconds
Completed Kernels. #iterations=11
Done!
Finished running bfs on dsp.
Printing results...
Result stored in result.txt
ABCD : END


ABCD : WGSIZE : 8192
ABCD : INPUT : graph262144.txt
ABCD : NUM_I : 3
ABCD : ITERATION : 3
Reading File
no_of_nodes 262144 num_of_blocks 1 work_group_size 8192
Starting bfs on dsp...
Creating Buffers...
Buffers Created.
Setting values of buffers...
Done setting values.
Creating Kernels...
Kernels created.
Running Kernels...
ABCD : BFS : K1 : 23899 microseconds
ABCD : BFS : K2 : 715 microseconds
ABCD : BFS : K1 : 1551 microseconds
ABCD : BFS : K2 : 686 microseconds
ABCD : BFS : K1 : 1546 microseconds
ABCD : BFS : K2 : 822 microseconds
ABCD : BFS : K1 : 1799 microseconds
ABCD : BFS : K2 : 1621 microseconds
ABCD : BFS : K1 : 2884 microseconds
ABCD : BFS : K2 : 4845 microseconds
ABCD : BFS : K1 : 8108 microseconds
ABCD : BFS : K2 : 18673 microseconds
ABCD : BFS : K1 : 31274 microseconds
ABCD : BFS : K2 : 62381 microseconds
ABCD : BFS : K1 : 91761 microseconds
ABCD : BFS : K2 : 91080 microseconds
ABCD : BFS : K1 : 110378 microseconds
ABCD : BFS : K2 : 18779 microseconds
ABCD : BFS : K1 : 21403 microseconds
ABCD : BFS : K2 : 729 microseconds
ABCD : BFS : K1 : 1576 microseconds
ABCD : BFS : K2 : 652 microseconds
Completed Kernels. #iterations=11
Done!
Finished running bfs on dsp.
Printing results...
Result stored in result.txt
ABCD : END


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Input: graph262144.txt
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

ABCD : WGSIZE : 16384
ABCD : INPUT : graph262144.txt
ABCD : NUM_I : 3
ABCD : ITERATION : 1
Reading File
no_of_nodes 262144 num_of_blocks 1 work_group_size 16384
Starting bfs on dsp...
Creating Buffers...
Buffers Created.
Setting values of buffers...
Done setting values.
Creating Kernels...
Kernels created.
Running Kernels...
ABCD : BFS : K1 : 24570 microseconds
ABCD : BFS : K2 : 676 microseconds
ABCD : BFS : K1 : 1834 microseconds
ABCD : BFS : K2 : 685 microseconds
ABCD : BFS : K1 : 1859 microseconds
ABCD : BFS : K2 : 819 microseconds
ABCD : BFS : K1 : 2104 microseconds
ABCD : BFS : K2 : 1528 microseconds
ABCD : BFS : K1 : 3142 microseconds
ABCD : BFS : K2 : 4858 microseconds
ABCD : BFS : K1 : 8274 microseconds
ABCD : BFS : K2 : 18442 microseconds
ABCD : BFS : K1 : 31914 microseconds
ABCD : BFS : K2 : 62300 microseconds
ABCD : BFS : K1 : 92943 microseconds
ABCD : BFS : K2 : 90763 microseconds
ABCD : BFS : K1 : 110544 microseconds
ABCD : BFS : K2 : 18837 microseconds
ABCD : BFS : K1 : 22248 microseconds
ABCD : BFS : K2 : 714 microseconds
ABCD : BFS : K1 : 1894 microseconds
ABCD : BFS : K2 : 637 microseconds
Completed Kernels. #iterations=11
Done!
Finished running bfs on dsp.
Printing results...
Result stored in result.txt
ABCD : END


ABCD : WGSIZE : 16384
ABCD : INPUT : graph262144.txt
ABCD : NUM_I : 3
ABCD : ITERATION : 2
Reading File
no_of_nodes 262144 num_of_blocks 1 work_group_size 16384
Starting bfs on dsp...
Creating Buffers...
Buffers Created.
Setting values of buffers...
Done setting values.
Creating Kernels...
Kernels created.
Running Kernels...
ABCD : BFS : K1 : 24527 microseconds
ABCD : BFS : K2 : 678 microseconds
ABCD : BFS : K1 : 1830 microseconds
ABCD : BFS : K2 : 689 microseconds
ABCD : BFS : K1 : 1868 microseconds
ABCD : BFS : K2 : 819 microseconds
ABCD : BFS : K1 : 2100 microseconds
ABCD : BFS : K2 : 1524 microseconds
ABCD : BFS : K1 : 3149 microseconds
ABCD : BFS : K2 : 4856 microseconds
ABCD : BFS : K1 : 8304 microseconds
ABCD : BFS : K2 : 18428 microseconds
ABCD : BFS : K1 : 31770 microseconds
ABCD : BFS : K2 : 62226 microseconds
ABCD : BFS : K1 : 92946 microseconds
ABCD : BFS : K2 : 90766 microseconds
ABCD : BFS : K1 : 110598 microseconds
ABCD : BFS : K2 : 18823 microseconds
ABCD : BFS : K1 : 22246 microseconds
ABCD : BFS : K2 : 714 microseconds
ABCD : BFS : K1 : 1890 microseconds
ABCD : BFS : K2 : 636 microseconds
Completed Kernels. #iterations=11
Done!
Finished running bfs on dsp.
Printing results...
Result stored in result.txt
ABCD : END


ABCD : WGSIZE : 16384
ABCD : INPUT : graph262144.txt
ABCD : NUM_I : 3
ABCD : ITERATION : 3
Reading File
no_of_nodes 262144 num_of_blocks 1 work_group_size 16384
Starting bfs on dsp...
Creating Buffers...
Buffers Created.
Setting values of buffers...
Done setting values.
Creating Kernels...
Kernels created.
Running Kernels...
ABCD : BFS : K1 : 24550 microseconds
ABCD : BFS : K2 : 675 microseconds
ABCD : BFS : K1 : 1832 microseconds
ABCD : BFS : K2 : 684 microseconds
ABCD : BFS : K1 : 1871 microseconds
ABCD : BFS : K2 : 818 microseconds
ABCD : BFS : K1 : 2108 microseconds
ABCD : BFS : K2 : 1529 microseconds
ABCD : BFS : K1 : 3127 microseconds
ABCD : BFS : K2 : 4860 microseconds
ABCD : BFS : K1 : 8288 microseconds
ABCD : BFS : K2 : 18421 microseconds
ABCD : BFS : K1 : 31786 microseconds
ABCD : BFS : K2 : 62255 microseconds
ABCD : BFS : K1 : 93034 microseconds
ABCD : BFS : K2 : 90738 microseconds
ABCD : BFS : K1 : 110608 microseconds
ABCD : BFS : K2 : 18820 microseconds
ABCD : BFS : K1 : 22258 microseconds
ABCD : BFS : K2 : 714 microseconds
ABCD : BFS : K1 : 1894 microseconds
ABCD : BFS : K2 : 637 microseconds
Completed Kernels. #iterations=11
Done!
Finished running bfs on dsp.
Printing results...
Result stored in result.txt
ABCD : END


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Input: graph262144.txt
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

ABCD : WGSIZE : 32768
ABCD : INPUT : graph262144.txt
ABCD : NUM_I : 3
ABCD : ITERATION : 1
Reading File
no_of_nodes 262144 num_of_blocks 1 work_group_size 32768
Starting bfs on dsp...
Creating Buffers...
Buffers Created.
Setting values of buffers...
Done setting values.
Creating Kernels...
Kernels created.
Running Kernels...
ABCD : BFS : K1 : 26245 microseconds
ABCD : BFS : K2 : 672 microseconds
ABCD : BFS : K1 : 2828 microseconds
ABCD : BFS : K2 : 688 microseconds
ABCD : BFS : K1 : 2858 microseconds
ABCD : BFS : K2 : 824 microseconds
ABCD : BFS : K1 : 3039 microseconds
ABCD : BFS : K2 : 1522 microseconds
ABCD : BFS : K1 : 3976 microseconds
ABCD : BFS : K2 : 4918 microseconds
ABCD : BFS : K1 : 9619 microseconds
ABCD : BFS : K2 : 18329 microseconds
ABCD : BFS : K1 : 32326 microseconds
ABCD : BFS : K2 : 62506 microseconds
ABCD : BFS : K1 : 95257 microseconds
ABCD : BFS : K2 : 90863 microseconds
ABCD : BFS : K1 : 113207 microseconds
ABCD : BFS : K2 : 18766 microseconds
ABCD : BFS : K1 : 22637 microseconds
ABCD : BFS : K2 : 705 microseconds
ABCD : BFS : K1 : 2891 microseconds
ABCD : BFS : K2 : 635 microseconds
Completed Kernels. #iterations=11
Done!
Finished running bfs on dsp.
Printing results...
Result stored in result.txt
ABCD : END


ABCD : WGSIZE : 32768
ABCD : INPUT : graph262144.txt
ABCD : NUM_I : 3
ABCD : ITERATION : 2
Reading File
no_of_nodes 262144 num_of_blocks 1 work_group_size 32768
Starting bfs on dsp...
Creating Buffers...
Buffers Created.
Setting values of buffers...
Done setting values.
Creating Kernels...
Kernels created.
Running Kernels...
ABCD : BFS : K1 : 22447 microseconds
ABCD : BFS : K2 : 675 microseconds
ABCD : BFS : K1 : 2833 microseconds
ABCD : BFS : K2 : 689 microseconds
ABCD : BFS : K1 : 2846 microseconds
ABCD : BFS : K2 : 830 microseconds
ABCD : BFS : K1 : 3069 microseconds
ABCD : BFS : K2 : 1542 microseconds
ABCD : BFS : K1 : 4001 microseconds
ABCD : BFS : K2 : 4917 microseconds
ABCD : BFS : K1 : 9605 microseconds
ABCD : BFS : K2 : 18351 microseconds
ABCD : BFS : K1 : 32396 microseconds
ABCD : BFS : K2 : 62545 microseconds
ABCD : BFS : K1 : 95201 microseconds
ABCD : BFS : K2 : 90854 microseconds
ABCD : BFS : K1 : 113138 microseconds
ABCD : BFS : K2 : 18772 microseconds
ABCD : BFS : K1 : 22604 microseconds
ABCD : BFS : K2 : 704 microseconds
ABCD : BFS : K1 : 2880 microseconds
ABCD : BFS : K2 : 630 microseconds
Completed Kernels. #iterations=11
Done!
Finished running bfs on dsp.
Printing results...
Result stored in result.txt
ABCD : END


ABCD : WGSIZE : 32768
ABCD : INPUT : graph262144.txt
ABCD : NUM_I : 3
ABCD : ITERATION : 3
Reading File
no_of_nodes 262144 num_of_blocks 1 work_group_size 32768
Starting bfs on dsp...
Creating Buffers...
Buffers Created.
Setting values of buffers...
Done setting values.
Creating Kernels...
Kernels created.
Running Kernels...
ABCD : BFS : K1 : 25197 microseconds
ABCD : BFS : K2 : 669 microseconds
ABCD : BFS : K1 : 2831 microseconds
ABCD : BFS : K2 : 688 microseconds
ABCD : BFS : K1 : 2846 microseconds
ABCD : BFS : K2 : 826 microseconds
ABCD : BFS : K1 : 3039 microseconds
ABCD : BFS : K2 : 1523 microseconds
ABCD : BFS : K1 : 3974 microseconds
ABCD : BFS : K2 : 4921 microseconds
ABCD : BFS : K1 : 9623 microseconds
ABCD : BFS : K2 : 18355 microseconds
ABCD : BFS : K1 : 32352 microseconds
ABCD : BFS : K2 : 62549 microseconds
ABCD : BFS : K1 : 95254 microseconds
ABCD : BFS : K2 : 90870 microseconds
ABCD : BFS : K1 : 113181 microseconds
ABCD : BFS : K2 : 18755 microseconds
ABCD : BFS : K1 : 22586 microseconds
ABCD : BFS : K2 : 704 microseconds
ABCD : BFS : K1 : 2871 microseconds
ABCD : BFS : K2 : 634 microseconds
Completed Kernels. #iterations=11
Done!
Finished running bfs on dsp.
Printing results...
Result stored in result.txt
ABCD : END


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Input: graph262144.txt
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

ABCD : WGSIZE : 65536
ABCD : INPUT : graph262144.txt
ABCD : NUM_I : 3
ABCD : ITERATION : 1
Reading File
no_of_nodes 262144 num_of_blocks 1 work_group_size 65536
Starting bfs on dsp...
Creating Buffers...
Buffers Created.
Setting values of buffers...
Done setting values.
Creating Kernels...
Kernels created.
Running Kernels...
ABCD : BFS : K1 : 29879 microseconds
ABCD : BFS : K2 : 1203 microseconds
ABCD : BFS : K1 : 5452 microseconds
ABCD : BFS : K2 : 1242 microseconds
ABCD : BFS : K1 : 5566 microseconds
ABCD : BFS : K2 : 1508 microseconds
ABCD : BFS : K1 : 5826 microseconds
ABCD : BFS : K2 : 2819 microseconds
ABCD : BFS : K1 : 7769 microseconds
ABCD : BFS : K2 : 9098 microseconds
ABCD : BFS : K1 : 16592 microseconds
ABCD : BFS : K2 : 34920 microseconds
ABCD : BFS : K1 : 54059 microseconds
ABCD : BFS : K2 : 119282 microseconds
ABCD : BFS : K1 : 166320 microseconds
ABCD : BFS : K2 : 173882 microseconds
ABCD : BFS : K1 : 217180 microseconds
ABCD : BFS : K2 : 35572 microseconds
ABCD : BFS : K1 : 44732 microseconds
ABCD : BFS : K2 : 1303 microseconds
ABCD : BFS : K1 : 5596 microseconds
ABCD : BFS : K2 : 1158 microseconds
Completed Kernels. #iterations=11
Done!
Finished running bfs on dsp.
Printing results...
Result stored in result.txt
ABCD : END


ABCD : WGSIZE : 65536
ABCD : INPUT : graph262144.txt
ABCD : NUM_I : 3
ABCD : ITERATION : 2
Reading File
no_of_nodes 262144 num_of_blocks 1 work_group_size 65536
Starting bfs on dsp...
Creating Buffers...
Buffers Created.
Setting values of buffers...
Done setting values.
Creating Kernels...
Kernels created.
Running Kernels...
ABCD : BFS : K1 : 29939 microseconds
ABCD : BFS : K2 : 1205 microseconds
ABCD : BFS : K1 : 5472 microseconds
ABCD : BFS : K2 : 1240 microseconds
ABCD : BFS : K1 : 5566 microseconds
ABCD : BFS : K2 : 1506 microseconds
ABCD : BFS : K1 : 5817 microseconds
ABCD : BFS : K2 : 2814 microseconds
ABCD : BFS : K1 : 7749 microseconds
ABCD : BFS : K2 : 9098 microseconds
ABCD : BFS : K1 : 16585 microseconds
ABCD : BFS : K2 : 34895 microseconds
ABCD : BFS : K1 : 54044 microseconds
ABCD : BFS : K2 : 119249 microseconds
ABCD : BFS : K1 : 166342 microseconds
ABCD : BFS : K2 : 173896 microseconds
ABCD : BFS : K1 : 217291 microseconds
ABCD : BFS : K2 : 35569 microseconds
ABCD : BFS : K1 : 44763 microseconds
ABCD : BFS : K2 : 1306 microseconds
ABCD : BFS : K1 : 5597 microseconds
ABCD : BFS : K2 : 1158 microseconds
Completed Kernels. #iterations=11
Done!
Finished running bfs on dsp.
Printing results...
Result stored in result.txt
ABCD : END


ABCD : WGSIZE : 65536
ABCD : INPUT : graph262144.txt
ABCD : NUM_I : 3
ABCD : ITERATION : 3
Reading File
no_of_nodes 262144 num_of_blocks 1 work_group_size 65536
Starting bfs on dsp...
Creating Buffers...
Buffers Created.
Setting values of buffers...
Done setting values.
Creating Kernels...
Kernels created.
Running Kernels...
ABCD : BFS : K1 : 29921 microseconds
ABCD : BFS : K2 : 1204 microseconds
ABCD : BFS : K1 : 5471 microseconds
ABCD : BFS : K2 : 1238 microseconds
ABCD : BFS : K1 : 5552 microseconds
ABCD : BFS : K2 : 1508 microseconds
ABCD : BFS : K1 : 5841 microseconds
ABCD : BFS : K2 : 2815 microseconds
ABCD : BFS : K1 : 7747 microseconds
ABCD : BFS : K2 : 9097 microseconds
ABCD : BFS : K1 : 16566 microseconds
ABCD : BFS : K2 : 34893 microseconds
ABCD : BFS : K1 : 54003 microseconds
ABCD : BFS : K2 : 119252 microseconds
ABCD : BFS : K1 : 166232 microseconds
ABCD : BFS : K2 : 174009 microseconds
ABCD : BFS : K1 : 217298 microseconds
ABCD : BFS : K2 : 35569 microseconds
ABCD : BFS : K1 : 44816 microseconds
ABCD : BFS : K2 : 1319 microseconds
ABCD : BFS : K1 : 5584 microseconds
ABCD : BFS : K2 : 1158 microseconds
Completed Kernels. #iterations=11
Done!
Finished running bfs on dsp.
Printing results...
Result stored in result.txt
ABCD : END


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Input: graph262144.txt
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

ABCD : WGSIZE : 131072
ABCD : INPUT : graph262144.txt
ABCD : NUM_I : 3
ABCD : ITERATION : 1
Reading File
no_of_nodes 262144 num_of_blocks 1 work_group_size 131072
Starting bfs on dsp...
Creating Buffers...
Buffers Created.
Setting values of buffers...
Done setting values.
Creating Kernels...
Kernels created.
Running Kernels...
ABCD : BFS : K1 : 27465 microseconds
ABCD : BFS : K2 : 2264 microseconds
ABCD : BFS : K1 : 4664 microseconds
ABCD : BFS : K2 : 2367 microseconds
ABCD : BFS : K1 : 4827 microseconds
ABCD : BFS : K2 : 2859 microseconds
ABCD : BFS : K1 : 5487 microseconds
ABCD : BFS : K2 : 5430 microseconds
ABCD : BFS : K1 : 8981 microseconds
ABCD : BFS : K2 : 17679 microseconds
ABCD : BFS : K1 : 25564 microseconds
ABCD : BFS : K2 : 68853 microseconds
ABCD : BFS : K1 : 95099 microseconds
ABCD : BFS : K2 : 234980 microseconds
ABCD : BFS : K1 : 306514 microseconds
ABCD : BFS : K2 : 342777 microseconds
ABCD : BFS : K1 : 409091 microseconds
ABCD : BFS : K2 : 69940 microseconds
ABCD : BFS : K1 : 80166 microseconds
ABCD : BFS : K2 : 2464 microseconds
ABCD : BFS : K1 : 4873 microseconds
ABCD : BFS : K2 : 2214 microseconds
Completed Kernels. #iterations=11
Done!
Finished running bfs on dsp.
Printing results...
Result stored in result.txt
ABCD : END


ABCD : WGSIZE : 131072
ABCD : INPUT : graph262144.txt
ABCD : NUM_I : 3
ABCD : ITERATION : 2
Reading File
no_of_nodes 262144 num_of_blocks 1 work_group_size 131072
Starting bfs on dsp...
Creating Buffers...
Buffers Created.
Setting values of buffers...
Done setting values.
Creating Kernels...
Kernels created.
Running Kernels...
ABCD : BFS : K1 : 31344 microseconds
ABCD : BFS : K2 : 2262 microseconds
ABCD : BFS : K1 : 4647 microseconds
ABCD : BFS : K2 : 2366 microseconds
ABCD : BFS : K1 : 4813 microseconds
ABCD : BFS : K2 : 2864 microseconds
ABCD : BFS : K1 : 5505 microseconds
ABCD : BFS : K2 : 5432 microseconds
ABCD : BFS : K1 : 8995 microseconds
ABCD : BFS : K2 : 17691 microseconds
ABCD : BFS : K1 : 25625 microseconds
ABCD : BFS : K2 : 68869 microseconds
ABCD : BFS : K1 : 95047 microseconds
ABCD : BFS : K2 : 234831 microseconds
ABCD : BFS : K1 : 306612 microseconds
ABCD : BFS : K2 : 342742 microseconds
ABCD : BFS : K1 : 409371 microseconds
ABCD : BFS : K2 : 69987 microseconds
ABCD : BFS : K1 : 80235 microseconds
ABCD : BFS : K2 : 2467 microseconds
ABCD : BFS : K1 : 4891 microseconds
ABCD : BFS : K2 : 2219 microseconds
Completed Kernels. #iterations=11
Done!
Finished running bfs on dsp.
Printing results...
Result stored in result.txt
ABCD : END


ABCD : WGSIZE : 131072
ABCD : INPUT : graph262144.txt
ABCD : NUM_I : 3
ABCD : ITERATION : 3
Reading File
no_of_nodes 262144 num_of_blocks 1 work_group_size 131072
Starting bfs on dsp...
Creating Buffers...
Buffers Created.
Setting values of buffers...
Done setting values.
Creating Kernels...
Kernels created.
Running Kernels...
ABCD : BFS : K1 : 30313 microseconds
ABCD : BFS : K2 : 2261 microseconds
ABCD : BFS : K1 : 4664 microseconds
ABCD : BFS : K2 : 2365 microseconds
ABCD : BFS : K1 : 4806 microseconds
ABCD : BFS : K2 : 2863 microseconds
ABCD : BFS : K1 : 5484 microseconds
ABCD : BFS : K2 : 5497 microseconds
ABCD : BFS : K1 : 8980 microseconds
ABCD : BFS : K2 : 17695 microseconds
ABCD : BFS : K1 : 25607 microseconds
ABCD : BFS : K2 : 68811 microseconds
ABCD : BFS : K1 : 95123 microseconds
ABCD : BFS : K2 : 234842 microseconds
ABCD : BFS : K1 : 306496 microseconds
ABCD : BFS : K2 : 343033 microseconds
ABCD : BFS : K1 : 409072 microseconds
ABCD : BFS : K2 : 69923 microseconds
ABCD : BFS : K1 : 80149 microseconds
ABCD : BFS : K2 : 2461 microseconds
ABCD : BFS : K1 : 4884 microseconds
ABCD : BFS : K2 : 2214 microseconds
Completed Kernels. #iterations=11
Done!
Finished running bfs on dsp.
Printing results...
Result stored in result.txt
ABCD : END


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Input: graph262144.txt
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

ABCD : WGSIZE : 262144
ABCD : INPUT : graph262144.txt
ABCD : NUM_I : 3
ABCD : ITERATION : 1
Reading File
no_of_nodes 262144 num_of_blocks 1 work_group_size 262144
Starting bfs on dsp...
Creating Buffers...
Buffers Created.
Setting values of buffers...
Done setting values.
Creating Kernels...
Kernels created.
Running Kernels...
ABCD : BFS : K1 : 39912 microseconds
ABCD : BFS : K2 : 4399 microseconds
ABCD : BFS : K1 : 9153 microseconds
ABCD : BFS : K2 : 4600 microseconds
ABCD : BFS : K1 : 9468 microseconds
ABCD : BFS : K2 : 5558 microseconds
ABCD : BFS : K1 : 10782 microseconds
ABCD : BFS : K2 : 10708 microseconds
ABCD : BFS : K1 : 17654 microseconds
ABCD : BFS : K2 : 34994 microseconds
ABCD : BFS : K1 : 50204 microseconds
ABCD : BFS : K2 : 136079 microseconds
ABCD : BFS : K1 : 185367 microseconds
ABCD : BFS : K2 : 468035 microseconds
ABCD : BFS : K1 : 602994 microseconds
ABCD : BFS : K2 : 681941 microseconds
ABCD : BFS : K1 : 809866 microseconds
ABCD : BFS : K2 : 138519 microseconds
ABCD : BFS : K1 : 158430 microseconds
ABCD : BFS : K2 : 4788 microseconds
ABCD : BFS : K1 : 9615 microseconds
ABCD : BFS : K2 : 4349 microseconds
Completed Kernels. #iterations=11
Done!
Finished running bfs on dsp.
Printing results...
Result stored in result.txt
ABCD : END


ABCD : WGSIZE : 262144
ABCD : INPUT : graph262144.txt
ABCD : NUM_I : 3
ABCD : ITERATION : 2
Reading File
no_of_nodes 262144 num_of_blocks 1 work_group_size 262144
Starting bfs on dsp...
Creating Buffers...
Buffers Created.
Setting values of buffers...
Done setting values.
Creating Kernels...
Kernels created.
Running Kernels...
ABCD : BFS : K1 : 39930 microseconds
ABCD : BFS : K2 : 4401 microseconds
ABCD : BFS : K1 : 9170 microseconds
ABCD : BFS : K2 : 4600 microseconds
ABCD : BFS : K1 : 9446 microseconds
ABCD : BFS : K2 : 5564 microseconds
ABCD : BFS : K1 : 10766 microseconds
ABCD : BFS : K2 : 10706 microseconds
ABCD : BFS : K1 : 17679 microseconds
ABCD : BFS : K2 : 34998 microseconds
ABCD : BFS : K1 : 50197 microseconds
ABCD : BFS : K2 : 136067 microseconds
ABCD : BFS : K1 : 185466 microseconds
ABCD : BFS : K2 : 468018 microseconds
ABCD : BFS : K1 : 602874 microseconds
ABCD : BFS : K2 : 681906 microseconds
ABCD : BFS : K1 : 810022 microseconds
ABCD : BFS : K2 : 138488 microseconds
ABCD : BFS : K1 : 158433 microseconds
ABCD : BFS : K2 : 4785 microseconds
ABCD : BFS : K1 : 9582 microseconds
ABCD : BFS : K2 : 4334 microseconds
Completed Kernels. #iterations=11
Done!
Finished running bfs on dsp.
Printing results...
Result stored in result.txt
ABCD : END


ABCD : WGSIZE : 262144
ABCD : INPUT : graph262144.txt
ABCD : NUM_I : 3
ABCD : ITERATION : 3
Reading File
no_of_nodes 262144 num_of_blocks 1 work_group_size 262144
Starting bfs on dsp...
Creating Buffers...
Buffers Created.
Setting values of buffers...
Done setting values.
Creating Kernels...
Kernels created.
Running Kernels...
ABCD : BFS : K1 : 29673 microseconds
ABCD : BFS : K2 : 4396 microseconds
ABCD : BFS : K1 : 9174 microseconds
ABCD : BFS : K2 : 4599 microseconds
ABCD : BFS : K1 : 9462 microseconds
ABCD : BFS : K2 : 5564 microseconds
ABCD : BFS : K1 : 10772 microseconds
ABCD : BFS : K2 : 10707 microseconds
ABCD : BFS : K1 : 17668 microseconds
ABCD : BFS : K2 : 35025 microseconds
ABCD : BFS : K1 : 50223 microseconds
ABCD : BFS : K2 : 136060 microseconds
ABCD : BFS : K1 : 185414 microseconds
ABCD : BFS : K2 : 468092 microseconds
ABCD : BFS : K1 : 602921 microseconds
ABCD : BFS : K2 : 682196 microseconds
ABCD : BFS : K1 : 809977 microseconds
ABCD : BFS : K2 : 138520 microseconds
ABCD : BFS : K1 : 158435 microseconds
ABCD : BFS : K2 : 4788 microseconds
ABCD : BFS : K1 : 9582 microseconds
ABCD : BFS : K2 : 4333 microseconds
Completed Kernels. #iterations=11
Done!
Finished running bfs on dsp.
Printing results...
Result stored in result.txt
ABCD : END


