/******************************************************************************
 * Copyright (c) 2013-2014, Texas Instruments Incorporated - http://www.ti.com/
 *   All rights reserved.
 *
 *   Redistribution and use in source and binary forms, with or without
 *   modification, are permitted provided that the following conditions are met:
 *       * Redistributions of source code must retain the above copyright
 *         notice, this list of conditions and the following disclaimer.
 *       * Redistributions in binary form must reproduce the above copyright
 *         notice, this list of conditions and the following disclaimer in the
 *         documentation and/or other materials provided with the distribution.
 *       * Neither the name of Texas Instruments Incorporated nor the
 *         names of its contributors may be used to endorse or promote products
 *         derived from this software without specific prior written permission.
 *
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 *   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 *   THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************/
#define __CL_ENABLE_EXCEPTIONS
#include <CL/cl.hpp>
#include <iostream>
#include <cstdlib>
#include <cassert>
#include <assert.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "ocl_util.h"
#include <sys/time.h>

// xzl -- what does this mean?
//#define MAX_THREADS_PER_BLOCK 512

int work_group_size = 262144;

#define MAX_THREADS_PER_BLOCK 262144


/*
#ifdef RD_WG_SIZE_0_0                                                            
	#define work_group_size RD_WG_SIZE_0_0
#else
	#define work_group_size 256
#endif
*/
using namespace cl;

struct Node
{
	int starting;		// xzl: id of the 1st edge leaving this node
	int no_of_edges;
};

//----------------------------------------------------------
//--bfs on cpu
//--programmer:	jianbin
//--date:	26/01/2011
//--note: width is changed to the new_width
//----------------------------------------------------------
void run_bfs_cpu(int no_of_nodes, Node *h_graph_nodes, int edge_list_size, \
		int *h_graph_edges, bool *h_graph_mask, bool *h_updating_graph_mask, \
		bool *h_graph_visited, int *h_cost_ref){
	bool stop;
	int k = 0;
	do{
		//if no thread changes this value then the loop stops
		stop=false;

		//printf("==================== iteration %d ker1 ==============\n", k);

		for(int tid = 0; tid < no_of_nodes; tid++ )
		{
			if (h_graph_mask[tid] == true){
				//printf("ker1: working on node %d\n", tid);	// xzl
				h_graph_mask[tid]=false;
				for(int i=h_graph_nodes[tid].starting; i<(h_graph_nodes[tid].no_of_edges + h_graph_nodes[tid].starting); i++){
					int id = h_graph_edges[i];	//--cambine: node id is connected with node tid
					if(!h_graph_visited[id]){	//--cambine: if node id has not been visited, enter the body below
						h_cost_ref[id]=h_cost_ref[tid]+1;
						h_updating_graph_mask[id]=true;
					}
				}
			}
		}

		printf("==================== iteration %d ker2 ==============\n", k);

  		for(int tid=0; tid< no_of_nodes ; tid++ )
		{
			if (h_updating_graph_mask[tid] == true){
				//printf("ker2: working on node %d\n", tid);	// xzl
				h_graph_mask[tid]=true;
				h_graph_visited[tid]=true;
				stop=true;
				h_updating_graph_mask[tid]=false;
			}
		}
		k++;
	}
	while(stop);
	//printf("xzl: done. iterations %d\n", k);
}

void run_bfs_dsp(int no_of_nodes, Node *h_graph_nodes, int edge_list_size, \
                 int *h_graph_edges, bool *h_graph_mask, bool *h_updating_graph_mask, \
                 bool *h_graph_visited, int *h_cost)
throw(std::string)
{
	signal(SIGABRT, exit);
	signal(SIGTERM, exit);
	int i = 0;
	bool h_over;

	int h_time_index = 0;
	
     	unsigned long long int * h_timestamp = (unsigned long long int*) malloc(sizeof(unsigned long long int) * work_group_size * 2);
	
	for(i = 0; i < work_group_size * 2; i++)
	{
		h_timestamp[i] = 0;
	}
	
	try {
		//Create Context
		Context             context(CL_DEVICE_TYPE_ACCELERATOR);
		std::vector<Device> devices = context.getInfo<CL_CONTEXT_DEVICES>();

		//Create Buffers
		printf("Creating Buffers...\n");
		Buffer 		d_graph_nodes (context, CL_MEM_READ_ONLY,  no_of_nodes*sizeof(Node));
		Buffer		d_graph_edges (context, CL_MEM_READ_ONLY, edge_list_size*sizeof(int));
		Buffer		d_graph_mask (context, CL_MEM_READ_ONLY, no_of_nodes*sizeof(bool));
		Buffer		d_updating_graph_mask (context, CL_MEM_READ_ONLY, (no_of_nodes*sizeof(bool)));
		Buffer		d_graph_visited (context, CL_MEM_READ_ONLY, (no_of_nodes*sizeof(bool)));
		Buffer		d_cost (context, CL_MEM_READ_WRITE, (no_of_nodes*sizeof(int)));
		Buffer		d_over (context, CL_MEM_READ_WRITE, sizeof(bool));
		Buffer		d_no_of_nodes (context, CL_MEM_READ_ONLY, sizeof(int));
		Buffer		d_timestamp (context, CL_MEM_READ_WRITE|CL_MEM_USE_MSMC_TI, sizeof(unsigned long long int)*work_group_size*2);
		printf("Buffers Created.\n");
#if 0
		//Get source and compile
		printf("Compiling kernels...\n");
		Program::Sources    source(1, std::make_pair(kernStr, strlen(kernStr)));
		Program             program = Program(context, source);
		program.build(devices);

		Program::Sources    bfs1Src(1, std::make_pair(bfs1Str, strlen(bfs1Str)));
		Program		    program2 = Program(context, bfs1Src);
		program2.build(devices);
		printf("bfs1 kernel compiled.\n");

		Program::Sources    bfs2Src(1, std::make_pair(bfs2Str, strlen(bfs2Str)));
		Program		    program3 = Program(context, bfs2Src);
		program3.build(devices);
		printf("bfs2 kernel compiled.\n");
		printf("Done compiling kernels.\n");
#endif

		// load compiled kernels
		char *bin;
		int bin_length = ocl_read_binary("bfs.out", bin);

		Program::Binaries   binary(1, std::make_pair(bin, bin_length));
		Program             program = Program(context, devices, binary);
		program.build(devices);

		delete [] bin;

		//Create Command Queue
		CommandQueue  Q (context, devices[0]);

		//Set value of buffers
		printf("Setting values of buffers...\n");
		Q.enqueueWriteBuffer(d_graph_nodes, CL_FALSE, 0, no_of_nodes * sizeof(Node), h_graph_nodes);
		Q.enqueueWriteBuffer(d_graph_edges, CL_FALSE, 0, edge_list_size * sizeof(int), h_graph_edges);
		Q.enqueueWriteBuffer(d_graph_mask, CL_FALSE, 0, no_of_nodes * sizeof(bool), h_graph_mask);
		Q.enqueueWriteBuffer(d_updating_graph_mask, CL_FALSE, 0, no_of_nodes * sizeof(bool), h_updating_graph_mask);
		Q.enqueueWriteBuffer(d_graph_visited, CL_FALSE, 0, no_of_nodes * sizeof(bool), h_graph_visited);
		Q.enqueueWriteBuffer(d_cost, CL_FALSE, 0, no_of_nodes * sizeof(int), h_cost);
		Q.enqueueWriteBuffer(d_no_of_nodes, CL_FALSE, 0, sizeof(int), &no_of_nodes);
		Q.enqueueWriteBuffer(d_timestamp, CL_FALSE, 0, sizeof(unsigned long long int) * work_group_size * 2, h_timestamp);
		printf("Done setting values.\n");

		//Create Kernel
		printf("Creating Kernels...\n");
//		Kernel        K (program, "devset");
//		Kernel	      bfs1 (program2, "BFS_1");
//		Kernel	      bfs2 (program3, "BFS_2");

		Kernel	      bfs1 (program, "BFS_1");
		Kernel	      bfs2 (program, "BFS_2");

		printf("Kernels created.\n");

		//Run Kernel
		printf("Running Kernels...\n");

        struct timeval tv_start;
        struct timeval tv_end;
     //   gettimeofday(&tv_start, NULL);


		int k = 0; // xzl
		do {
			if (k > 50) {
				printf("xzl: break\n");
				break;
			}

			//printf("==================== iteration %d ker1 ==============\n", k);

			h_over = false;
			Q.enqueueWriteBuffer(d_over, CL_FALSE, 0, sizeof(bool), &h_over);

                gettimeofday(&tv_start, NULL);
			KernelFunctor BFS_1 = bfs1.bind(Q, NDRange(no_of_nodes), NDRange(work_group_size));
			BFS_1(d_graph_nodes, d_graph_edges, d_graph_mask, d_updating_graph_mask, d_graph_visited, d_cost, d_no_of_nodes, d_timestamp).wait();
                        
                gettimeofday(&tv_end, NULL);
                  printf("ABCD : BFS : K1 : %ld microseconds\n", (tv_end.tv_sec*1000000 + tv_end.tv_usec) - (tv_start.tv_sec * 1000000 + tv_start.tv_usec));


        gettimeofday(&tv_start, NULL);
			//printf("==================== iteration %d ker2 ==============\n", k);

			KernelFunctor BFS_2 = bfs2.bind(Q, NDRange(no_of_nodes), NDRange(work_group_size));
			BFS_2(d_graph_mask, d_updating_graph_mask, d_graph_visited, d_over, d_no_of_nodes).wait();
//			printf("iteration %d: ker2 done\n", i);

               gettimeofday(&tv_end, NULL);
                  printf("ABCD : BFS : K2 : %ld microseconds\n", (tv_end.tv_sec*1000000 + tv_end.tv_usec) - (tv_start.tv_sec * 1000000 + tv_start.tv_usec));
			Q.enqueueReadBuffer(d_over, CL_TRUE, 0, sizeof(bool), &h_over);

			k++;
           
		Q.enqueueReadBuffer(d_timestamp, CL_TRUE, 0, sizeof(unsigned long long int) * work_group_size * 2, h_timestamp);
	/*	for(i = 0; i < work_group_size * 2; i+=2)
		{
			printf(" offload: %d :local_id: %d  start: %llu  end: %llu\n",k, i/2, h_timestamp[i], h_timestamp[i+1]);
		}
        */

		} while (h_over);
 
       // 	gettimeofday(&tv_end, NULL);
        //	printf("Elapsed time: %ld microseconds\n", (tv_end.tv_sec*1000000 + tv_end.tv_usec) - (tv_start.tv_sec * 1000000 + tv_start.tv_usec));


		printf("Completed Kernels. #iterations=%d\n", k);

		//Get Result from device
		Q.enqueueReadBuffer(d_cost, CL_TRUE, 0, no_of_nodes * sizeof(int), h_cost);
	

	} catch (Error err) {
		std::cerr <<"ERROR: " <<err.what() <<"(" <<err.err() <<")" <<std::endl;
	}

	//for (int i = 0; i < size; ++i) assert(ary[i] == 'x');
	std::cout << "Done!" << std::endl;
}

void Usage(int argc, char**argv)
{
	fprintf(stderr,"Usage: %s <input_file>\n", argv[0]);
}

int main(int argc, char * argv[])
{
	int no_of_nodes;
	int edge_list_size;
	FILE *fp;
	Node* h_graph_nodes;
	bool *h_graph_mask, *h_updating_graph_mask, *h_graph_visited;
	int ret;
	try {
		char *input_f;
		if(argc!=3) {
			Usage(argc, argv);
			exit(0);
		}

		input_f = argv[1];
                work_group_size = atoi(argv[2]);
		printf("Reading File\n");
		//Read in Graph from a file
		fp = fopen(input_f,"r");
		if(!fp) {
			printf("Error Reading graph file\n");
			return 0;
		}

		int source = 0;

		ret = fscanf(fp,"%d",&no_of_nodes);

		int num_of_blocks = 1;
		int num_of_threads_per_block = no_of_nodes;

		//Make execution Parameters according to the number of nodes
		//Distribute threads across multiple Blocks if necessary
		if(no_of_nodes>MAX_THREADS_PER_BLOCK) {
			num_of_blocks = (int)ceil(no_of_nodes/(double)MAX_THREADS_PER_BLOCK);
			num_of_threads_per_block = MAX_THREADS_PER_BLOCK;
		}
		//work_group_size = num_of_threads_per_block;

		// xzl: dump info: # nodes, etc.
		printf("no_of_nodes %d num_of_blocks %d work_group_size %d\n",
				no_of_nodes, num_of_blocks, work_group_size);

		// allocate host memory
		h_graph_nodes = (Node*) malloc(sizeof(Node)*no_of_nodes);
		assert(h_graph_nodes);

		h_graph_mask = (bool*) malloc(sizeof(bool)*no_of_nodes);
		assert(h_graph_mask);

		h_updating_graph_mask = (bool*) malloc(sizeof(bool)*no_of_nodes);
		assert(h_updating_graph_mask);

		h_graph_visited = (bool*) malloc(sizeof(bool)*no_of_nodes);
		assert(h_graph_visited);

		// xzl -- load the graph from the file
		// input file format (see graphgen.cpp)
		//
		// #nodes
		// #edges(accumulative so far)  #edges_for_node_i
		// ...
		//
		// start_node_id
		//
		// node_0_edge_0_dest	node_0_edge_0_weight
		// node_0_edge_1_dest	node_0_edge_1_weight
		// ...
		// node_n_edge_m_dest	node_n_edge_m_weight
		//

		int start, edgeno;
		// initalize the memory
		for(int i = 0; i < no_of_nodes; i++) {
			ret = fscanf(fp,"%d %d",&start,&edgeno);
			h_graph_nodes[i].starting = start;
			h_graph_nodes[i].no_of_edges = edgeno;
			h_graph_mask[i]=false;
			h_updating_graph_mask[i]=false;
			h_graph_visited[i]=false;
		}
		//read the source node from the file
		ret = fscanf(fp,"%d",&source);
		source=0;
		//set the source node as true in the mask
		h_graph_mask[source]=true;
		h_graph_visited[source]=true;
		ret = fscanf(fp,"%d",&edge_list_size);

		int id,cost;
		// xzl: each element is one edge.
		// element i: dest of edge i
		// cost is unused? why?
		int* h_graph_edges = (int*) malloc(sizeof(int)*edge_list_size);
		for(int i=0; i < edge_list_size ; i++) {
			ret = fscanf(fp,"%d",&id);
			ret = fscanf(fp,"%d",&cost);
			h_graph_edges[i] = id;
		}

		if(fp)
			fclose(fp);

		// xzl: h_cost is the cost reaching every node
		// unreachable -1
		// source 0

		// allocate mem for the result on host side
		int	*h_cost = (int*) malloc(sizeof(int)*no_of_nodes);
		assert(h_cost);

		int *h_cost_ref = (int*)malloc(sizeof(int)*no_of_nodes);
		assert(h_cost_ref);

		for(int i=0;i<no_of_nodes;i++) {
			h_cost[i]=-1;
			h_cost_ref[i] = -1;
		}
		h_cost[source]=0;
		h_cost_ref[source]=0;

		//---------------------------------------------------------
		// Call run_bfs_dsp

#if 1
		printf("Starting bfs on dsp...\n");
		run_bfs_dsp(no_of_nodes,h_graph_nodes,edge_list_size,h_graph_edges,
				h_graph_mask, h_updating_graph_mask, h_graph_visited, h_cost);
		printf("Finished running bfs on dsp.\n");
#endif

#if 0	// xzl: XXX init var again
		printf("Starting bfs on cpu...\n");
		run_bfs_cpu(no_of_nodes,h_graph_nodes,edge_list_size,h_graph_edges,
				h_graph_mask, h_updating_graph_mask, h_graph_visited, h_cost);
#endif
		int i = 0;
		printf("Printing results...\n");
		FILE *fpo = fopen("result.txt","w");
		for(i = 0;i<no_of_nodes;i++)
			fprintf(fpo,"%d) cost:%d\n",i,h_cost[i]);
		fclose(fpo);
		printf("Result stored in result.txt\n");
		
		
		//release host memory
		free(h_graph_nodes);
		free(h_graph_mask);
		free(h_updating_graph_mask);
		free(h_graph_visited);

	} catch(std::string msg) {
		std::cout<<"--cambine: exception in main ->"<<msg<<std::endl;
		//release host memory
		free(h_graph_nodes);
		free(h_graph_mask);
		free(h_updating_graph_mask);
		free(h_graph_visited);
	}

	return 0;
}
