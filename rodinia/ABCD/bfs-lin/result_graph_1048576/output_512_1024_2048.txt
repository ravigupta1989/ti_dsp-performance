~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Input: graph1048576.txt
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

ABCD : WGSIZE : 512
ABCD : INPUT : graph1048576.txt
ABCD : NUM_I : 3
ABCD : ITERATION : 1
Reading File
no_of_nodes 1048576 num_of_blocks 4 work_group_size 512
Starting bfs on dsp...
Creating Buffers...
Buffers Created.
Setting values of buffers...
Done setting values.
Creating Kernels...
Kernels created.
Running Kernels...
ABCD : BFS : K1 : 92950 microseconds
ABCD : BFS : K2 : 2937 microseconds
ABCD : BFS : K1 : 5500 microseconds
ABCD : BFS : K2 : 2990 microseconds
ABCD : BFS : K1 : 5620 microseconds
ABCD : BFS : K2 : 3571 microseconds
ABCD : BFS : K1 : 6341 microseconds
ABCD : BFS : K2 : 5399 microseconds
ABCD : BFS : K1 : 8960 microseconds
ABCD : BFS : K2 : 12159 microseconds
ABCD : BFS : K1 : 20442 microseconds
ABCD : BFS : K2 : 42819 microseconds
ABCD : BFS : K1 : 73522 microseconds
ABCD : BFS : K2 : 162938 microseconds
ABCD : BFS : K1 : 257076 microseconds
ABCD : BFS : K2 : 394128 microseconds
ABCD : BFS : K1 : 500387 microseconds
ABCD : BFS : K2 : 227530 microseconds
ABCD : BFS : K1 : 255485 microseconds
ABCD : BFS : K2 : 9454 microseconds
ABCD : BFS : K1 : 12397 microseconds
ABCD : BFS : K2 : 2900 microseconds
Completed Kernels. #iterations=11
Done!
Finished running bfs on dsp.
Printing results...
Result stored in result.txt
ABCD : END


ABCD : WGSIZE : 512
ABCD : INPUT : graph1048576.txt
ABCD : NUM_I : 3
ABCD : ITERATION : 2
Reading File
no_of_nodes 1048576 num_of_blocks 4 work_group_size 512
Starting bfs on dsp...
Creating Buffers...
Buffers Created.
Setting values of buffers...
Done setting values.
Creating Kernels...
Kernels created.
Running Kernels...
ABCD : BFS : K1 : 93169 microseconds
ABCD : BFS : K2 : 2933 microseconds
ABCD : BFS : K1 : 5499 microseconds
ABCD : BFS : K2 : 2993 microseconds
ABCD : BFS : K1 : 5625 microseconds
ABCD : BFS : K2 : 3577 microseconds
ABCD : BFS : K1 : 6354 microseconds
ABCD : BFS : K2 : 5399 microseconds
ABCD : BFS : K1 : 8954 microseconds
ABCD : BFS : K2 : 12150 microseconds
ABCD : BFS : K1 : 20406 microseconds
ABCD : BFS : K2 : 42820 microseconds
ABCD : BFS : K1 : 73549 microseconds
ABCD : BFS : K2 : 162952 microseconds
ABCD : BFS : K1 : 256973 microseconds
ABCD : BFS : K2 : 393312 microseconds
ABCD : BFS : K1 : 500353 microseconds
ABCD : BFS : K2 : 227526 microseconds
ABCD : BFS : K1 : 255427 microseconds
ABCD : BFS : K2 : 9459 microseconds
ABCD : BFS : K1 : 12392 microseconds
ABCD : BFS : K2 : 2897 microseconds
Completed Kernels. #iterations=11
Done!
Finished running bfs on dsp.
Printing results...
Result stored in result.txt
ABCD : END


ABCD : WGSIZE : 512
ABCD : INPUT : graph1048576.txt
ABCD : NUM_I : 3
ABCD : ITERATION : 3
Reading File
no_of_nodes 1048576 num_of_blocks 4 work_group_size 512
Starting bfs on dsp...
Creating Buffers...
Buffers Created.
Setting values of buffers...
Done setting values.
Creating Kernels...
Kernels created.
Running Kernels...
ABCD : BFS : K1 : 92572 microseconds
ABCD : BFS : K2 : 2932 microseconds
ABCD : BFS : K1 : 5492 microseconds
ABCD : BFS : K2 : 2993 microseconds
ABCD : BFS : K1 : 5606 microseconds
ABCD : BFS : K2 : 3572 microseconds
ABCD : BFS : K1 : 6352 microseconds
ABCD : BFS : K2 : 5398 microseconds
ABCD : BFS : K1 : 8953 microseconds
ABCD : BFS : K2 : 12158 microseconds
ABCD : BFS : K1 : 20411 microseconds
ABCD : BFS : K2 : 42824 microseconds
ABCD : BFS : K1 : 73531 microseconds
ABCD : BFS : K2 : 162972 microseconds
ABCD : BFS : K1 : 257095 microseconds
ABCD : BFS : K2 : 393309 microseconds
ABCD : BFS : K1 : 500392 microseconds
ABCD : BFS : K2 : 227557 microseconds
ABCD : BFS : K1 : 255564 microseconds
ABCD : BFS : K2 : 9454 microseconds
ABCD : BFS : K1 : 12402 microseconds
ABCD : BFS : K2 : 2900 microseconds
Completed Kernels. #iterations=11
Done!
Finished running bfs on dsp.
Printing results...
Result stored in result.txt
ABCD : END


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Input: graph1048576.txt
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

ABCD : WGSIZE : 1024
ABCD : INPUT : graph1048576.txt
ABCD : NUM_I : 3
ABCD : ITERATION : 1
Reading File
no_of_nodes 1048576 num_of_blocks 4 work_group_size 1024
Starting bfs on dsp...
Creating Buffers...
Buffers Created.
Setting values of buffers...
Done setting values.
Creating Kernels...
Kernels created.
Running Kernels...
ABCD : BFS : K1 : 89713 microseconds
ABCD : BFS : K2 : 2585 microseconds
ABCD : BFS : K1 : 4975 microseconds
ABCD : BFS : K2 : 2632 microseconds
ABCD : BFS : K1 : 5095 microseconds
ABCD : BFS : K2 : 3160 microseconds
ABCD : BFS : K1 : 5746 microseconds
ABCD : BFS : K2 : 4722 microseconds
ABCD : BFS : K1 : 8048 microseconds
ABCD : BFS : K2 : 10911 microseconds
ABCD : BFS : K1 : 18815 microseconds
ABCD : BFS : K2 : 39631 microseconds
ABCD : BFS : K1 : 70557 microseconds
ABCD : BFS : K2 : 152806 microseconds
ABCD : BFS : K1 : 248045 microseconds
ABCD : BFS : K2 : 370060 microseconds
ABCD : BFS : K1 : 474844 microseconds
ABCD : BFS : K2 : 213721 microseconds
ABCD : BFS : K1 : 239529 microseconds
ABCD : BFS : K2 : 8372 microseconds
ABCD : BFS : K1 : 11105 microseconds
ABCD : BFS : K2 : 2552 microseconds
Completed Kernels. #iterations=11
Done!
Finished running bfs on dsp.
Printing results...
Result stored in result.txt
ABCD : END


ABCD : WGSIZE : 1024
ABCD : INPUT : graph1048576.txt
ABCD : NUM_I : 3
ABCD : ITERATION : 2
Reading File
no_of_nodes 1048576 num_of_blocks 4 work_group_size 1024
Starting bfs on dsp...
Creating Buffers...
Buffers Created.
Setting values of buffers...
Done setting values.
Creating Kernels...
Kernels created.
Running Kernels...
ABCD : BFS : K1 : 94894 microseconds
ABCD : BFS : K2 : 2587 microseconds
ABCD : BFS : K1 : 4988 microseconds
ABCD : BFS : K2 : 2629 microseconds
ABCD : BFS : K1 : 5093 microseconds
ABCD : BFS : K2 : 3163 microseconds
ABCD : BFS : K1 : 5747 microseconds
ABCD : BFS : K2 : 4724 microseconds
ABCD : BFS : K1 : 8040 microseconds
ABCD : BFS : K2 : 10923 microseconds
ABCD : BFS : K1 : 18793 microseconds
ABCD : BFS : K2 : 39625 microseconds
ABCD : BFS : K1 : 70485 microseconds
ABCD : BFS : K2 : 152778 microseconds
ABCD : BFS : K1 : 248091 microseconds
ABCD : BFS : K2 : 370220 microseconds
ABCD : BFS : K1 : 473998 microseconds
ABCD : BFS : K2 : 213629 microseconds
ABCD : BFS : K1 : 239409 microseconds
ABCD : BFS : K2 : 8379 microseconds
ABCD : BFS : K1 : 11095 microseconds
ABCD : BFS : K2 : 2555 microseconds
Completed Kernels. #iterations=11
Done!
Finished running bfs on dsp.
Printing results...
Result stored in result.txt
ABCD : END


ABCD : WGSIZE : 1024
ABCD : INPUT : graph1048576.txt
ABCD : NUM_I : 3
ABCD : ITERATION : 3
Reading File
no_of_nodes 1048576 num_of_blocks 4 work_group_size 1024
Starting bfs on dsp...
Creating Buffers...
Buffers Created.
Setting values of buffers...
Done setting values.
Creating Kernels...
Kernels created.
Running Kernels...
ABCD : BFS : K1 : 92553 microseconds
ABCD : BFS : K2 : 2584 microseconds
ABCD : BFS : K1 : 4990 microseconds
ABCD : BFS : K2 : 2632 microseconds
ABCD : BFS : K1 : 5079 microseconds
ABCD : BFS : K2 : 3165 microseconds
ABCD : BFS : K1 : 5751 microseconds
ABCD : BFS : K2 : 4730 microseconds
ABCD : BFS : K1 : 8028 microseconds
ABCD : BFS : K2 : 10911 microseconds
ABCD : BFS : K1 : 18804 microseconds
ABCD : BFS : K2 : 39637 microseconds
ABCD : BFS : K1 : 70579 microseconds
ABCD : BFS : K2 : 152764 microseconds
ABCD : BFS : K1 : 248065 microseconds
ABCD : BFS : K2 : 370328 microseconds
ABCD : BFS : K1 : 474017 microseconds
ABCD : BFS : K2 : 213668 microseconds
ABCD : BFS : K1 : 239541 microseconds
ABCD : BFS : K2 : 8379 microseconds
ABCD : BFS : K1 : 11099 microseconds
ABCD : BFS : K2 : 2551 microseconds
Completed Kernels. #iterations=11
Done!
Finished running bfs on dsp.
Printing results...
Result stored in result.txt
ABCD : END


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Input: graph1048576.txt
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

ABCD : WGSIZE : 2048
ABCD : INPUT : graph1048576.txt
ABCD : NUM_I : 3
ABCD : ITERATION : 1
Reading File
no_of_nodes 1048576 num_of_blocks 4 work_group_size 2048
Starting bfs on dsp...
Creating Buffers...
Buffers Created.
Setting values of buffers...
Done setting values.
Creating Kernels...
Kernels created.
Running Kernels...
ABCD : BFS : K1 : 92717 microseconds
ABCD : BFS : K2 : 2436 microseconds
ABCD : BFS : K1 : 4827 microseconds
ABCD : BFS : K2 : 2478 microseconds
ABCD : BFS : K1 : 4959 microseconds
ABCD : BFS : K2 : 2974 microseconds
ABCD : BFS : K1 : 5579 microseconds
ABCD : BFS : K2 : 4449 microseconds
ABCD : BFS : K1 : 7752 microseconds
ABCD : BFS : K2 : 10578 microseconds
ABCD : BFS : K1 : 18370 microseconds
ABCD : BFS : K2 : 39341 microseconds
ABCD : BFS : K1 : 70231 microseconds
ABCD : BFS : K2 : 152325 microseconds
ABCD : BFS : K1 : 247658 microseconds
ABCD : BFS : K2 : 369213 microseconds
ABCD : BFS : K1 : 475196 microseconds
ABCD : BFS : K2 : 212481 microseconds
ABCD : BFS : K1 : 239397 microseconds
ABCD : BFS : K2 : 8071 microseconds
ABCD : BFS : K1 : 10795 microseconds
ABCD : BFS : K2 : 2403 microseconds
Completed Kernels. #iterations=11
Done!
Finished running bfs on dsp.
Printing results...
Result stored in result.txt
ABCD : END


ABCD : WGSIZE : 2048
ABCD : INPUT : graph1048576.txt
ABCD : NUM_I : 3
ABCD : ITERATION : 2
Reading File
no_of_nodes 1048576 num_of_blocks 4 work_group_size 2048
Starting bfs on dsp...
Creating Buffers...
Buffers Created.
Setting values of buffers...
Done setting values.
Creating Kernels...
Kernels created.
Running Kernels...
ABCD : BFS : K1 : 92203 microseconds
ABCD : BFS : K2 : 2435 microseconds
ABCD : BFS : K1 : 4807 microseconds
ABCD : BFS : K2 : 2475 microseconds
ABCD : BFS : K1 : 4954 microseconds
ABCD : BFS : K2 : 2972 microseconds
ABCD : BFS : K1 : 5605 microseconds
ABCD : BFS : K2 : 4446 microseconds
ABCD : BFS : K1 : 7748 microseconds
ABCD : BFS : K2 : 10578 microseconds
ABCD : BFS : K1 : 18381 microseconds
ABCD : BFS : K2 : 39328 microseconds
ABCD : BFS : K1 : 70197 microseconds
ABCD : BFS : K2 : 152425 microseconds
ABCD : BFS : K1 : 247187 microseconds
ABCD : BFS : K2 : 369144 microseconds
ABCD : BFS : K1 : 475080 microseconds
ABCD : BFS : K2 : 212484 microseconds
ABCD : BFS : K1 : 238857 microseconds
ABCD : BFS : K2 : 8050 microseconds
ABCD : BFS : K1 : 10776 microseconds
ABCD : BFS : K2 : 2407 microseconds
Completed Kernels. #iterations=11
Done!
Finished running bfs on dsp.
Printing results...
Result stored in result.txt
ABCD : END


ABCD : WGSIZE : 2048
ABCD : INPUT : graph1048576.txt
ABCD : NUM_I : 3
ABCD : ITERATION : 3
Reading File
no_of_nodes 1048576 num_of_blocks 4 work_group_size 2048
Starting bfs on dsp...
Creating Buffers...
Buffers Created.
Setting values of buffers...
Done setting values.
Creating Kernels...
Kernels created.
Running Kernels...
ABCD : BFS : K1 : 103672 microseconds
ABCD : BFS : K2 : 2436 microseconds
ABCD : BFS : K1 : 4829 microseconds
ABCD : BFS : K2 : 2476 microseconds
ABCD : BFS : K1 : 4944 microseconds
ABCD : BFS : K2 : 2967 microseconds
ABCD : BFS : K1 : 5591 microseconds
ABCD : BFS : K2 : 4448 microseconds
ABCD : BFS : K1 : 7755 microseconds
ABCD : BFS : K2 : 10562 microseconds
ABCD : BFS : K1 : 18373 microseconds
ABCD : BFS : K2 : 39328 microseconds
ABCD : BFS : K1 : 70281 microseconds
ABCD : BFS : K2 : 152320 microseconds
ABCD : BFS : K1 : 247586 microseconds
ABCD : BFS : K2 : 369143 microseconds
ABCD : BFS : K1 : 475172 microseconds
ABCD : BFS : K2 : 212478 microseconds
ABCD : BFS : K1 : 238707 microseconds
ABCD : BFS : K2 : 8056 microseconds
ABCD : BFS : K1 : 10790 microseconds
ABCD : BFS : K2 : 2403 microseconds
Completed Kernels. #iterations=11
Done!
Finished running bfs on dsp.
Printing results...
Result stored in result.txt
ABCD : END


