typedef struct
{
	int starting;
	int no_of_edges;
} Node;

#ifdef DEBUG
#define myprintf printf
#else
#define myprintf(arg...)
#endif

// xzl:
// g_graph_mask: marking is done. need to explore (+ update cost)
// g_updating_graph_mask: nodes just discovered. needs to do marking.
//
kernel void BFS_1( const global Node* g_graph_nodes,
                   const global int* g_graph_edges,
                   global bool* g_graph_mask,
                   global bool* g_updating_graph_mask,
                   global bool* g_graph_visited,
                   global int* g_cost,
                   const  int no_of_nodes,
		   global unsigned long int * g_timestamp)
{
	// xzl: workitem id in the global scope.
	// tid is the node the workitem is working on
	int groupid = get_group_id(0);
	int localid = get_local_id(0);		
	int tid = get_global_id(0);
	
	if(groupid == 2)
	{
		g_timestamp[localid*2] = __clock64();
		
		// uncomment the following line to see the timestamp printed on all cores.

//		printf("start: %d : %llu  \n", get_local_id(0), g_timestamp[localid*2]);
	}		
/*
	if(__core_num() == 0)
	{
		tim = __clock();
		g_timestamp[tid] = tim;
	}
*/
	if( tid<no_of_nodes && g_graph_mask[tid]) {
		//myprintf("ker1: working on node %d\n", tid);

		// xzl: we're done exploring node tid
		g_graph_mask[tid]=false;
		// xzl: check all edges leaving the node
		for(int i=g_graph_nodes[tid].starting;
				i<(g_graph_nodes[tid].no_of_edges + g_graph_nodes[tid].starting);
				i++) {
			int id = g_graph_edges[i]; // xzl: id: dest node of the edge
//			printf("ker1: destnode is %d\n", id);
			if(!g_graph_visited[id]) {
				g_cost[id]=g_cost[tid]+1; // xzl: seems default edge cost = 1
				g_updating_graph_mask[id]=true; // xzl: going to explore these nodes
			}
		}

		// xzl: apparently this is too expensive. XXX
		
		__cache_l1d_flush();
		__cache_l2_flush();
		
	}
	if(groupid == 2)
	{
		g_timestamp[localid*2 + 1] = __clock64();
//	        printf("end  : %d : %llu  \n", get_local_id(0), g_timestamp[localid*2 + 1]);
	}
}

// xzl: extra nodes are discovered in ker1 and they need to be marked.
// ker2 re-distributes the workload of marking
kernel void BFS_2(global bool* g_graph_mask,
                  global bool* g_updating_graph_mask,
                   global bool* g_graph_visited,
                  global bool* g_over,
                  const  int no_of_nodes)
{
	int tid = get_global_id(0);

	if( tid<no_of_nodes && g_updating_graph_mask[tid]) {
		//myprintf("ker2: working on node %d\n", tid);

		g_graph_mask[tid]=true;
		g_graph_visited[tid]=true;
		*g_over=true;		// xzl: telling the host code: need more iterations
		g_updating_graph_mask[tid]=false;

		// xzl: apparently this is too expensive. XXX
		__cache_l1d_flush();
		__cache_l2_flush();
	}
}
