#ifndef FLT_MAX
#define FLT_MAX 3.40282347e+38
#endif

__kernel void
kmeans_kernel_c(__global float  *feature,   
			  __global float  *clusters,
			  __global int    *membership,
			    int     npoints,
				int     nclusters,
				int     nfeatures,
				int		offset,
				int		size
			  ) 
{

//__cache_l2_128k();
	unsigned int point_id = get_global_id(0);

//if (point_id == 0)
//	{ __cache_l2_256k();}

    int index = 0;
//printf("KC\n");
    //const unsigned int point_id = get_global_id(0);
		if (point_id < npoints)
		{
			float min_dist=FLT_MAX;
			for (int i=0; i < nclusters; i++) {
				
				float dist = 0;
				float ans  = 0;
				for (int l=0; l<nfeatures; l++){
						ans += (feature[l * npoints + point_id]-clusters[i*nfeatures+l])* 
							   (feature[l * npoints + point_id]-clusters[i*nfeatures+l]);
				}

				dist = ans;
				if (dist < min_dist) {
					min_dist = dist;
					index    = i;
					
				}
			}
//		  printf("K2 :%d\n", index);
		  membership[point_id] = index;
		}	
	
	return;
}

__kernel void
kmeans_swap(__global float  *feature,   
			__global float  *feature_swap,
			int     npoints,
			int     nfeatures
){

//__cache_l2_512k();
//printf("K1 :global =  %d \n",get_global_size(0)); 
	unsigned int tid = get_global_id(0);
	for(int i = 0; i <  nfeatures; i++)
		feature_swap[i * npoints + tid] = feature[tid * nfeatures + i];

} 
