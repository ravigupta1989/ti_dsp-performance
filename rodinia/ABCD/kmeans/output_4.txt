~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Input: 819200.txt
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

ABCD : WGSIZE : 4
ABCD : INPUT : 819200.txt
ABCD : NUM_I : 3
ABCD : ITERATION : 1
WG size of kernel_swap = 4, WG size of kernel_kmeans = 4 

I/O completed

Number of objects: 819200
Number of features: 34
ravi allocate called 
n_points= 819200 
ABCD : KMEANS : K1 : 394 microseconds
ravi kmeansOCL called 
n_points= 819200 
ABCD : KMEANS : K2  : 6448766 microseconds
ravi kmeansOCL called 
n_points= 819200 
ABCD : KMEANS : K2  : 6445949 microseconds
ravi kmeansOCL called 
n_points= 819200 
ABCD : KMEANS : K2  : 6443730 microseconds
ravi kmeansOCL called 
n_points= 819200 
ABCD : KMEANS : K2  : 6448529 microseconds
ravi kmeansOCL called 
n_points= 819200 
ABCD : KMEANS : K2  : 6447186 microseconds
ravi kmeansOCL called 
n_points= 819200 
ABCD : KMEANS : K2  : 6449207 microseconds
ravi kmeansOCL called 
n_points= 819200 
ABCD : KMEANS : K2  : 6449227 microseconds
ravi kmeansOCL called 
n_points= 819200 
ABCD : KMEANS : K2  : 6445381 microseconds
ravi kmeansOCL called 
n_points= 819200 
ABCD : KMEANS : K2  : 6446825 microseconds
ravi kmeansOCL called 
n_points= 819200 
ABCD : KMEANS : K2  : 6445248 microseconds
ravi kmeansOCL called 
n_points= 819200 
ABCD : KMEANS : K2  : 6448112 microseconds
ravi kmeansOCL called 
n_points= 819200 
ABCD : KMEANS : K2  : 6442406 microseconds
ravi kmeansOCL called 
n_points= 819200 
ABCD : KMEANS : K2  : 6448247 microseconds
ravi kmeansOCL called 
n_points= 819200 
ABCD : KMEANS : K2  : 6453445 microseconds
ravi kmeansOCL called 
n_points= 819200 
ABCD : KMEANS : K2  : 6441406 microseconds
ravi kmeansOCL called 
n_points= 819200 
ABCD : KMEANS : K2  : 6445831 microseconds
ravi kmeansOCL called 
n_points= 819200 
ABCD : KMEANS : K2  : 6447185 microseconds
ravi kmeansOCL called 
n_points= 819200 
ABCD : KMEANS : K2  : 6446603 microseconds
ravi kmeansOCL called 
n_points= 819200 
ABCD : KMEANS : K2  : 6443110 microseconds
ravi kmeansOCL called 
n_points= 819200 
ABCD : KMEANS : K2  : 6449044 microseconds
iterated 20 times

================= Centroid Coordinates =================
0: 175.36 2209991.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 1.71 1.64 0.00 0.00 0.07 0.07 0.98 0.05 0.00 115.36 54.50 0.34 0.05 0.34 0.03 0.07 0.00 0.02 0.00

1: 5051.68 5134229.50 0.00 0.00 0.00 2.71 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 1.02 1.46 0.02 0.02 0.00 0.00 0.99 0.02 0.06 31.59 35.05 0.89 0.02 0.93 0.10 0.01 0.02 0.00 0.00

2: 56.28 715.76 230.61 0.01 0.00 0.03 0.00 0.00 0.00 0.00 0.01 0.00 0.00 0.00 0.00 376.83 337.55 0.16 0.16 0.06 0.06 0.80 0.02 0.02 244.34 192.23 0.76 0.03 0.69 0.00 0.16 0.16 0.06 0.06

3: 1985.80 701.20 3525758.75 0.00 0.00 0.00 0.60 3.60 0.20 0.40 1.80 0.00 0.00 0.00 0.00 1.00 1.00 0.00 0.00 0.00 0.00 1.00 0.00 0.00 164.20 69.40 0.31 0.04 0.01 0.10 0.15 0.17 0.15 0.00

4: 931.24 655.00 767574.38 0.00 0.00 0.00 0.00 13.38 0.00 0.05 13.24 0.05 0.00 0.00 0.00 1.43 2.05 0.02 0.00 0.00 0.00 1.00 0.00 0.01 159.29 55.38 0.26 0.03 0.01 0.01 0.02 0.02 0.01 0.03

Number of Iteration: 1
ABCD : END


ABCD : WGSIZE : 4
ABCD : INPUT : 819200.txt
ABCD : NUM_I : 3
ABCD : ITERATION : 2
WG size of kernel_swap = 4, WG size of kernel_kmeans = 4 

I/O completed

Number of objects: 819200
Number of features: 34
ravi allocate called 
n_points= 819200 
ABCD : KMEANS : K1 : 369 microseconds
ravi kmeansOCL called 
n_points= 819200 
ABCD : KMEANS : K2  : 6447509 microseconds
ravi kmeansOCL called 
n_points= 819200 
ABCD : KMEANS : K2  : 6449149 microseconds
ravi kmeansOCL called 
n_points= 819200 
ABCD : KMEANS : K2  : 6451064 microseconds
ravi kmeansOCL called 
n_points= 819200 
ABCD : KMEANS : K2  : 6448495 microseconds
ravi kmeansOCL called 
n_points= 819200 
ABCD : KMEANS : K2  : 6443723 microseconds
ravi kmeansOCL called 
n_points= 819200 
ABCD : KMEANS : K2  : 6450925 microseconds
ravi kmeansOCL called 
n_points= 819200 
ABCD : KMEANS : K2  : 6444872 microseconds
ravi kmeansOCL called 
n_points= 819200 
ABCD : KMEANS : K2  : 6447036 microseconds
ravi kmeansOCL called 
n_points= 819200 
ABCD : KMEANS : K2  : 6443007 microseconds
ravi kmeansOCL called 
n_points= 819200 
ABCD : KMEANS : K2  : 6447191 microseconds
ravi kmeansOCL called 
n_points= 819200 
ABCD : KMEANS : K2  : 6440789 microseconds
ravi kmeansOCL called 
n_points= 819200 
ABCD : KMEANS : K2  : 6449216 microseconds
ravi kmeansOCL called 
n_points= 819200 
ABCD : KMEANS : K2  : 6452156 microseconds
ravi kmeansOCL called 
n_points= 819200 
ABCD : KMEANS : K2  : 6441892 microseconds
ravi kmeansOCL called 
n_points= 819200 
ABCD : KMEANS : K2  : 6449748 microseconds
ravi kmeansOCL called 
n_points= 819200 
ABCD : KMEANS : K2  : 6451300 microseconds
ravi kmeansOCL called 
n_points= 819200 
ABCD : KMEANS : K2  : 6448010 microseconds
ravi kmeansOCL called 
n_points= 819200 
ABCD : KMEANS : K2  : 6445936 microseconds
ravi kmeansOCL called 
n_points= 819200 
ABCD : KMEANS : K2  : 6449817 microseconds
ravi kmeansOCL called 
n_points= 819200 
ABCD : KMEANS : K2  : 6446841 microseconds
iterated 20 times

================= Centroid Coordinates =================
0: 175.36 2209991.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 1.71 1.64 0.00 0.00 0.07 0.07 0.98 0.05 0.00 115.36 54.50 0.34 0.05 0.34 0.03 0.07 0.00 0.02 0.00

1: 5051.68 5134229.50 0.00 0.00 0.00 2.71 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 1.02 1.46 0.02 0.02 0.00 0.00 0.99 0.02 0.06 31.59 35.05 0.89 0.02 0.93 0.10 0.01 0.02 0.00 0.00

2: 56.28 715.76 230.61 0.01 0.00 0.03 0.00 0.00 0.00 0.00 0.01 0.00 0.00 0.00 0.00 376.83 337.55 0.16 0.16 0.06 0.06 0.80 0.02 0.02 244.34 192.23 0.76 0.03 0.69 0.00 0.16 0.16 0.06 0.06

3: 1985.80 701.20 3525758.75 0.00 0.00 0.00 0.60 3.60 0.20 0.40 1.80 0.00 0.00 0.00 0.00 1.00 1.00 0.00 0.00 0.00 0.00 1.00 0.00 0.00 164.20 69.40 0.31 0.04 0.01 0.10 0.15 0.17 0.15 0.00

4: 931.24 655.00 767574.38 0.00 0.00 0.00 0.00 13.38 0.00 0.05 13.24 0.05 0.00 0.00 0.00 1.43 2.05 0.02 0.00 0.00 0.00 1.00 0.00 0.01 159.29 55.38 0.26 0.03 0.01 0.01 0.02 0.02 0.01 0.03

Number of Iteration: 1
ABCD : END


ABCD : WGSIZE : 4
ABCD : INPUT : 819200.txt
ABCD : NUM_I : 3
ABCD : ITERATION : 3
WG size of kernel_swap = 4, WG size of kernel_kmeans = 4 

I/O completed

Number of objects: 819200
Number of features: 34
ravi allocate called 
n_points= 819200 
ABCD : KMEANS : K1 : 373 microseconds
ravi kmeansOCL called 
n_points= 819200 
ABCD : KMEANS : K2  : 6447201 microseconds
ravi kmeansOCL called 
n_points= 819200 
ABCD : KMEANS : K2  : 6444147 microseconds
ravi kmeansOCL called 
n_points= 819200 
ABCD : KMEANS : K2  : 6449617 microseconds
ravi kmeansOCL called 
n_points= 819200 
ABCD : KMEANS : K2  : 6450827 microseconds
ravi kmeansOCL called 
n_points= 819200 
ABCD : KMEANS : K2  : 6445644 microseconds
ravi kmeansOCL called 
n_points= 819200 
ABCD : KMEANS : K2  : 6450939 microseconds
ravi kmeansOCL called 
n_points= 819200 
ABCD : KMEANS : K2  : 6451687 microseconds
ravi kmeansOCL called 
n_points= 819200 
ABCD : KMEANS : K2  : 6450632 microseconds
ravi kmeansOCL called 
n_points= 819200 
ABCD : KMEANS : K2  : 6447654 microseconds
ravi kmeansOCL called 
n_points= 819200 
ABCD : KMEANS : K2  : 6442879 microseconds
ravi kmeansOCL called 
n_points= 819200 
ABCD : KMEANS : K2  : 6444505 microseconds
ravi kmeansOCL called 
n_points= 819200 
ABCD : KMEANS : K2  : 6448811 microseconds
ravi kmeansOCL called 
n_points= 819200 
ABCD : KMEANS : K2  : 6448364 microseconds
ravi kmeansOCL called 
n_points= 819200 
ABCD : KMEANS : K2  : 6442285 microseconds
ravi kmeansOCL called 
n_points= 819200 
ABCD : KMEANS : K2  : 6448294 microseconds
ravi kmeansOCL called 
n_points= 819200 
ABCD : KMEANS : K2  : 6442626 microseconds
ravi kmeansOCL called 
n_points= 819200 
ABCD : KMEANS : K2  : 6444415 microseconds
ravi kmeansOCL called 
n_points= 819200 
ABCD : KMEANS : K2  : 6442681 microseconds
ravi kmeansOCL called 
n_points= 819200 
ABCD : KMEANS : K2  : 6442489 microseconds
ravi kmeansOCL called 
n_points= 819200 
ABCD : KMEANS : K2  : 6444009 microseconds
iterated 20 times

================= Centroid Coordinates =================
0: 175.36 2209991.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 1.71 1.64 0.00 0.00 0.07 0.07 0.98 0.05 0.00 115.36 54.50 0.34 0.05 0.34 0.03 0.07 0.00 0.02 0.00

1: 5051.68 5134229.50 0.00 0.00 0.00 2.71 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 1.02 1.46 0.02 0.02 0.00 0.00 0.99 0.02 0.06 31.59 35.05 0.89 0.02 0.93 0.10 0.01 0.02 0.00 0.00

2: 56.28 715.76 230.61 0.01 0.00 0.03 0.00 0.00 0.00 0.00 0.01 0.00 0.00 0.00 0.00 376.83 337.55 0.16 0.16 0.06 0.06 0.80 0.02 0.02 244.34 192.23 0.76 0.03 0.69 0.00 0.16 0.16 0.06 0.06

3: 1985.80 701.20 3525758.75 0.00 0.00 0.00 0.60 3.60 0.20 0.40 1.80 0.00 0.00 0.00 0.00 1.00 1.00 0.00 0.00 0.00 0.00 1.00 0.00 0.00 164.20 69.40 0.31 0.04 0.01 0.10 0.15 0.17 0.15 0.00

4: 931.24 655.00 767574.38 0.00 0.00 0.00 0.00 13.38 0.00 0.05 13.24 0.05 0.00 0.00 0.00 1.43 2.05 0.02 0.00 0.00 0.00 1.00 0.00 0.01 159.29 55.38 0.26 0.03 0.01 0.01 0.02 0.02 0.01 0.03

Number of Iteration: 1
ABCD : END


