~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Input: 8192
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

ABCD : WGSIZE : 256
ABCD : INPUT : 8192
ABCD : NUM_I : 3
ABCD : ITERATION : 1
WG size of kernel = 256 
num_devices = 1
worksize = 8192
Processing upper-left matrix
1 
256 1 
ABCD : NW : K1 : 5215 microseconds
2 
512 1 
ABCD : NW : K1 : 71 microseconds
3 
768 1 
ABCD : NW : K1 : 37 microseconds
4 
1024 1 
ABCD : NW : K1 : 38 microseconds
5 
1280 1 
ABCD : NW : K1 : 31 microseconds
6 
1536 1 
ABCD : NW : K1 : 70 microseconds
7 
1792 1 
ABCD : NW : K1 : 33 microseconds
8 
2048 1 
ABCD : NW : K1 : 30 microseconds
9 
2304 1 
ABCD : NW : K1 : 30 microseconds
10 
2560 1 
ABCD : NW : K1 : 30 microseconds
11 
2816 1 
ABCD : NW : K1 : 29 microseconds
12 
3072 1 
ABCD : NW : K1 : 29 microseconds
13 
3328 1 
ABCD : NW : K1 : 29 microseconds
14 
3584 1 
ABCD : NW : K1 : 30 microseconds
15 
3840 1 
ABCD : NW : K1 : 29 microseconds
16 
4096 1 
ABCD : NW : K1 : 36 microseconds
17 
4352 1 
ABCD : NW : K1 : 29 microseconds
18 
4608 1 
ABCD : NW : K1 : 28 microseconds
19 
4864 1 
ABCD : NW : K1 : 30 microseconds
20 
5120 1 
ABCD : NW : K1 : 29 microseconds
21 
5376 1 
ABCD : NW : K1 : 28 microseconds
22 
5632 1 
ABCD : NW : K1 : 28 microseconds
23 
5888 1 
ABCD : NW : K1 : 28 microseconds
24 
6144 1 
ABCD : NW : K1 : 30 microseconds
25 
6400 1 
ABCD : NW : K1 : 36 microseconds
26 
6656 1 
ABCD : NW : K1 : 29 microseconds
27 
6912 1 
ABCD : NW : K1 : 28 microseconds
28 
7168 1 
ABCD : NW : K1 : 29 microseconds
29 
7424 1 
ABCD : NW : K1 : 28 microseconds
30 
7680 1 
ABCD : NW : K1 : 29 microseconds
31 
7936 1 
ABCD : NW : K1 : 29 microseconds
32 
8192 1 
ABCD : NW : K1 : 30 microseconds
Processing lower-right matrix
ABCD : NW : K2 : 46 microseconds
ABCD : NW : K2 : 26 microseconds
ABCD : NW : K2 : 24 microseconds
ABCD : NW : K2 : 43 microseconds
ABCD : NW : K2 : 21 microseconds
ABCD : NW : K2 : 21 microseconds
ABCD : NW : K2 : 21 microseconds
ABCD : NW : K2 : 31 microseconds
ABCD : NW : K2 : 27 microseconds
ABCD : NW : K2 : 20 microseconds
ABCD : NW : K2 : 20 microseconds
ABCD : NW : K2 : 40 microseconds
ABCD : NW : K2 : 29 microseconds
ABCD : NW : K2 : 19 microseconds
ABCD : NW : K2 : 19 microseconds
ABCD : NW : K2 : 19 microseconds
ABCD : NW : K2 : 19 microseconds
ABCD : NW : K2 : 20 microseconds
ABCD : NW : K2 : 30 microseconds
ABCD : NW : K2 : 19 microseconds
ABCD : NW : K2 : 20 microseconds
ABCD : NW : K2 : 20 microseconds
ABCD : NW : K2 : 20 microseconds
ABCD : NW : K2 : 19 microseconds
ABCD : NW : K2 : 31 microseconds
ABCD : NW : K2 : 57 microseconds
ABCD : NW : K2 : 20 microseconds
ABCD : NW : K2 : 20 microseconds
ABCD : NW : K2 : 19 microseconds
ABCD : NW : K2 : 19 microseconds
ABCD : NW : K2 : 20 microseconds
Computation Done
ABCD : END


ABCD : WGSIZE : 256
ABCD : INPUT : 8192
ABCD : NUM_I : 3
ABCD : ITERATION : 2
WG size of kernel = 256 
num_devices = 1
worksize = 8192
Processing upper-left matrix
1 
256 1 
ABCD : NW : K1 : 427 microseconds
2 
512 1 
ABCD : NW : K1 : 71 microseconds
3 
768 1 
ABCD : NW : K1 : 39 microseconds
4 
1024 1 
ABCD : NW : K1 : 38 microseconds
5 
1280 1 
ABCD : NW : K1 : 30 microseconds
6 
1536 1 
ABCD : NW : K1 : 30 microseconds
7 
1792 1 
ABCD : NW : K1 : 30 microseconds
8 
2048 1 
ABCD : NW : K1 : 31 microseconds
9 
2304 1 
ABCD : NW : K1 : 29 microseconds
10 
2560 1 
ABCD : NW : K1 : 29 microseconds
11 
2816 1 
ABCD : NW : K1 : 30 microseconds
12 
3072 1 
ABCD : NW : K1 : 29 microseconds
13 
3328 1 
ABCD : NW : K1 : 29 microseconds
14 
3584 1 
ABCD : NW : K1 : 29 microseconds
15 
3840 1 
ABCD : NW : K1 : 29 microseconds
16 
4096 1 
ABCD : NW : K1 : 37 microseconds
17 
4352 1 
ABCD : NW : K1 : 28 microseconds
18 
4608 1 
ABCD : NW : K1 : 30 microseconds
19 
4864 1 
ABCD : NW : K1 : 30 microseconds
20 
5120 1 
ABCD : NW : K1 : 29 microseconds
21 
5376 1 
ABCD : NW : K1 : 29 microseconds
22 
5632 1 
ABCD : NW : K1 : 30 microseconds
23 
5888 1 
ABCD : NW : K1 : 30 microseconds
24 
6144 1 
ABCD : NW : K1 : 29 microseconds
25 
6400 1 
ABCD : NW : K1 : 35 microseconds
26 
6656 1 
ABCD : NW : K1 : 29 microseconds
27 
6912 1 
ABCD : NW : K1 : 29 microseconds
28 
7168 1 
ABCD : NW : K1 : 29 microseconds
29 
7424 1 
ABCD : NW : K1 : 29 microseconds
30 
7680 1 
ABCD : NW : K1 : 29 microseconds
31 
7936 1 
ABCD : NW : K1 : 29 microseconds
32 
8192 1 
ABCD : NW : K1 : 29 microseconds
Processing lower-right matrix
ABCD : NW : K2 : 43 microseconds
ABCD : NW : K2 : 26 microseconds
ABCD : NW : K2 : 21 microseconds
ABCD : NW : K2 : 21 microseconds
ABCD : NW : K2 : 20 microseconds
ABCD : NW : K2 : 20 microseconds
ABCD : NW : K2 : 19 microseconds
ABCD : NW : K2 : 20 microseconds
ABCD : NW : K2 : 19 microseconds
ABCD : NW : K2 : 19 microseconds
ABCD : NW : K2 : 19 microseconds
ABCD : NW : K2 : 19 microseconds
ABCD : NW : K2 : 20 microseconds
ABCD : NW : K2 : 19 microseconds
ABCD : NW : K2 : 19 microseconds
ABCD : NW : K2 : 19 microseconds
ABCD : NW : K2 : 18 microseconds
ABCD : NW : K2 : 19 microseconds
ABCD : NW : K2 : 20 microseconds
ABCD : NW : K2 : 19 microseconds
ABCD : NW : K2 : 19 microseconds
ABCD : NW : K2 : 19 microseconds
ABCD : NW : K2 : 23 microseconds
ABCD : NW : K2 : 19 microseconds
ABCD : NW : K2 : 19 microseconds
ABCD : NW : K2 : 19 microseconds
ABCD : NW : K2 : 29 microseconds
ABCD : NW : K2 : 19 microseconds
ABCD : NW : K2 : 19 microseconds
ABCD : NW : K2 : 19 microseconds
ABCD : NW : K2 : 20 microseconds
Computation Done
ABCD : END


ABCD : WGSIZE : 256
ABCD : INPUT : 8192
ABCD : NUM_I : 3
ABCD : ITERATION : 3
WG size of kernel = 256 
num_devices = 1
worksize = 8192
Processing upper-left matrix
1 
256 1 
ABCD : NW : K1 : 426 microseconds
2 
512 1 
ABCD : NW : K1 : 71 microseconds
3 
768 1 
ABCD : NW : K1 : 35 microseconds
4 
1024 1 
ABCD : NW : K1 : 39 microseconds
5 
1280 1 
ABCD : NW : K1 : 30 microseconds
6 
1536 1 
ABCD : NW : K1 : 30 microseconds
7 
1792 1 
ABCD : NW : K1 : 30 microseconds
8 
2048 1 
ABCD : NW : K1 : 30 microseconds
9 
2304 1 
ABCD : NW : K1 : 31 microseconds
10 
2560 1 
ABCD : NW : K1 : 29 microseconds
11 
2816 1 
ABCD : NW : K1 : 29 microseconds
12 
3072 1 
ABCD : NW : K1 : 29 microseconds
13 
3328 1 
ABCD : NW : K1 : 29 microseconds
14 
3584 1 
ABCD : NW : K1 : 29 microseconds
15 
3840 1 
ABCD : NW : K1 : 29 microseconds
16 
4096 1 
ABCD : NW : K1 : 36 microseconds
17 
4352 1 
ABCD : NW : K1 : 29 microseconds
18 
4608 1 
ABCD : NW : K1 : 28 microseconds
19 
4864 1 
ABCD : NW : K1 : 30 microseconds
20 
5120 1 
ABCD : NW : K1 : 29 microseconds
21 
5376 1 
ABCD : NW : K1 : 29 microseconds
22 
5632 1 
ABCD : NW : K1 : 29 microseconds
23 
5888 1 
ABCD : NW : K1 : 28 microseconds
24 
6144 1 
ABCD : NW : K1 : 27 microseconds
25 
6400 1 
ABCD : NW : K1 : 36 microseconds
26 
6656 1 
ABCD : NW : K1 : 29 microseconds
27 
6912 1 
ABCD : NW : K1 : 28 microseconds
28 
7168 1 
ABCD : NW : K1 : 29 microseconds
29 
7424 1 
ABCD : NW : K1 : 28 microseconds
30 
7680 1 
ABCD : NW : K1 : 28 microseconds
31 
7936 1 
ABCD : NW : K1 : 30 microseconds
32 
8192 1 
ABCD : NW : K1 : 29 microseconds
Processing lower-right matrix
ABCD : NW : K2 : 40 microseconds
ABCD : NW : K2 : 25 microseconds
ABCD : NW : K2 : 22 microseconds
ABCD : NW : K2 : 21 microseconds
ABCD : NW : K2 : 19 microseconds
ABCD : NW : K2 : 20 microseconds
ABCD : NW : K2 : 26 microseconds
ABCD : NW : K2 : 20 microseconds
ABCD : NW : K2 : 20 microseconds
ABCD : NW : K2 : 19 microseconds
ABCD : NW : K2 : 19 microseconds
ABCD : NW : K2 : 19 microseconds
ABCD : NW : K2 : 20 microseconds
ABCD : NW : K2 : 19 microseconds
ABCD : NW : K2 : 19 microseconds
ABCD : NW : K2 : 41 microseconds
ABCD : NW : K2 : 25 microseconds
ABCD : NW : K2 : 34 microseconds
ABCD : NW : K2 : 20 microseconds
ABCD : NW : K2 : 19 microseconds
ABCD : NW : K2 : 19 microseconds
ABCD : NW : K2 : 19 microseconds
ABCD : NW : K2 : 20 microseconds
ABCD : NW : K2 : 19 microseconds
ABCD : NW : K2 : 19 microseconds
ABCD : NW : K2 : 20 microseconds
ABCD : NW : K2 : 19 microseconds
ABCD : NW : K2 : 18 microseconds
ABCD : NW : K2 : 18 microseconds
ABCD : NW : K2 : 19 microseconds
ABCD : NW : K2 : 20 microseconds
Computation Done
ABCD : END


