import subprocess
import sys
import os
import time
import string
import numpy

# the following values can be modified if necessary:
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

max_iterations = 20  #this is the maximum number of iterations that the script can handle
max_kernels = 10  # this is the maximum number of kernels (K1, K2, etc) that the script can handle

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


# initialize 2d matrix ~~~~~~~~~~~~~~~
kernel_times = []
kernel_num_exec = []
for i in range(0, max_iterations) :
	kernel_times.append([])
	kernel_num_exec.append([])
	for j in range(0, max_kernels+1) :
		kernel_times[i].append(0)
		kernel_num_exec[i].append(0)
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#initialize stats matrices~~~~~~~~~~~~
average_time = []
average_exec = []
stdev_time = []
stdev_exec = []

for i in range(0, max_kernels) :
	average_time.append(0)
	average_exec.append(0)
	stdev_time.append(0)
	stdev_exec.append(0)
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

filename = sys.argv[1];

f = open(filename, 'r')

temp_time = []
temp_exec = []
kernels = [' ', 'K1', 'K2', 'K3', 'K4', 'K5', 'K6', 'K7', 'K8', 'K9', 'K10']

for readLine in f :
	if len(readLine) >= 4 and (readLine[0] == 'A' and readLine[1] == 'B' and readLine[2] == 'C' and readLine[3] == 'D') :
		terms = readLine.split( )
		if terms[2] == 'END' :
			kernel_times[i-1].pop(0)
			kernel_num_exec[i-1].pop(0)
			#print("WGSIZE = " + wgsize)
			#print("INPUT = " + data)
			#print("ITERATION = " + iteration)
			#print kernel_times[i-1]
			#print kernel_num_exec[i-1]
			#print("\n")

			if iteration == str(num_iterations) :
				for j in range(0, 10) :
					for k in range(0, num_iterations) :
						temp_time.append(kernel_times[k][j])
						temp_exec.append(kernel_num_exec[k][j])
						#kernel_times[k] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
						#kernel_num_exec[k] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

					average_time[j] = sum(temp_time) / len(temp_time)
					average_exec[j] = sum(temp_exec) / len(temp_exec)
					
					stdev_time[j] = numpy.std(temp_time)
					stdev_exec[j] = numpy.std(temp_exec)
				
					temp_time = []
					temp_exec = []
				
				#print(average_time)
				#print(average_exec)
				print("WGSIZE = " + wgsize)
				print("Number of Iterations = " + str(num_iterations))
				print("Input data: " + data)
				print ', '.join(map(str, kernels))
				sys.stdout.write("Total Time, ")
				print ', '.join(map(str, average_time))
				sys.stdout.write("Num Executions, ")
				print ', '.join(map(str, average_exec))
				sys.stdout.write("Std Dev Time, ")
				print ', '.join(map(str, stdev_time))
				print("\n")
				
				#average_time = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
				#average_exec = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
				
				# initialize 2d matrix ~~~~~~~~~~~~~~~
				kernel_times = []
				kernel_num_exec = []
				for i in range(0, max_iterations) :
					kernel_times.append([])
					kernel_num_exec.append([])
					for j in range(0, max_kernels+1) :
						kernel_times[i].append(0)
						kernel_num_exec[i].append(0)
				#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

				#initialize stats matrices~~~~~~~~~~~~
				average_time = []
				average_exec = []
				stdev_time = []
				stdev_exec = []

				for i in range(0, max_kernels) :
					average_time.append(0)
					average_exec.append(0)
					stdev_time.append(0)
					stdev_exec.append(0)
				#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

			
		elif terms[2] == 'WGSIZE' :
			wgsize = terms[4]
		elif terms[2] == 'INPUT' :
			data = terms[4]
		elif terms[2] == 'ITERATION' :
			iteration = terms[4]
			i = int(iteration)
		elif terms[2] == 'NUM_I' :
			num_iterations = int(terms[4])
		else :
			app = terms[2]
			kernel = terms[4]
			time = terms[6]
	
			kernel_times[i-1][int(kernel[1])] = kernel_times[i-1][int(kernel[1])] + int(time)
			kernel_num_exec[i-1][int(kernel[1])] += 1;







#		if terms[2] != 'END' : 	#### Note : testing script must print "ABCD : END" when the interation ends
#			if terms[2] != 'WGSIZE' :
#				app = terms[2]
#				kernel = terms[4]
#				time = terms[6]
#		
#				kernel_times[int(kernel[1])] = kernel_times[int(kernel[1])] + int(time)
#				kernel_num_exec[int(kernel[1])] += 1;
#			else :
#				wgsize = terms[4]
#		else :
#			kernel_times.pop(0)
#			kernel_num_exec.pop(0)
#			print("WGSIZE = " + wgsize)
#			
#			print kernel_times
#			print kernel_num_exec
#			print "\n"
#			
#			kernel_times = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
#			kernel_num_exec = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

	




