~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Input: 128
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

ABCD : WGSIZE : 64
ABCD : INPUT : 128
ABCD : NUM_I : 3
ABCD : ITERATION : 1
WG size of kernel = 64 X 64
Platform: Texas Instruments, Inc.
Device: TI Multicore C66 DSP
Starting kernels...Block_rows= 3 
Block_Cols= 3 
ABCD : HOTSPOT : K1 : 4141 microseconds
ABCD : HOTSPOT : K1 : 1751 microseconds
ABCD : HOTSPOT : K1 : 1735 microseconds
ABCD : HOTSPOT : K1 : 1736 microseconds
ABCD : HOTSPOT : K1 : 1743 microseconds
ABCD : HOTSPOT : K1 : 1728 microseconds
ABCD : HOTSPOT : K1 : 1734 microseconds
ABCD : HOTSPOT : K1 : 1726 microseconds
ABCD : HOTSPOT : K1 : 1738 microseconds
ABCD : HOTSPOT : K1 : 1732 microseconds
ABCD : HOTSPOT : K1 : 1739 microseconds
ABCD : HOTSPOT : K1 : 1730 microseconds
ABCD : HOTSPOT : K1 : 1728 microseconds
ABCD : HOTSPOT : K1 : 1731 microseconds
ABCD : HOTSPOT : K1 : 1734 microseconds
ABCD : HOTSPOT : K1 : 1733 microseconds
ABCD : HOTSPOT : K1 : 1732 microseconds
ABCD : HOTSPOT : K1 : 1728 microseconds
ABCD : HOTSPOT : K1 : 1731 microseconds
ABCD : HOTSPOT : K1 : 1730 microseconds
ABCD : HOTSPOT : K1 : 1735 microseconds
ABCD : HOTSPOT : K1 : 1731 microseconds
ABCD : HOTSPOT : K1 : 1732 microseconds
ABCD : HOTSPOT : K1 : 1733 microseconds
ABCD : HOTSPOT : K1 : 1727 microseconds
ABCD : HOTSPOT : K1 : 1731 microseconds
ABCD : HOTSPOT : K1 : 1731 microseconds
ABCD : HOTSPOT : K1 : 1736 microseconds
ABCD : HOTSPOT : K1 : 1729 microseconds
ABCD : HOTSPOT : K1 : 1729 microseconds
ABCD : HOTSPOT : K1 : 1734 microseconds
ABCD : HOTSPOT : K1 : 1734 microseconds
ABCD : HOTSPOT : K1 : 1732 microseconds
ABCD : HOTSPOT : K1 : 1730 microseconds
ABCD : HOTSPOT : K1 : 1731 microseconds
ABCD : HOTSPOT : K1 : 1732 microseconds
ABCD : HOTSPOT : K1 : 1732 microseconds
ABCD : HOTSPOT : K1 : 1730 microseconds
ABCD : HOTSPOT : K1 : 1732 microseconds
ABCD : HOTSPOT : K1 : 1731 microseconds
ABCD : HOTSPOT : K1 : 1726 microseconds
ABCD : HOTSPOT : K1 : 1727 microseconds
ABCD : HOTSPOT : K1 : 1732 microseconds
ABCD : HOTSPOT : K1 : 1735 microseconds
ABCD : HOTSPOT : K1 : 1737 microseconds
ABCD : HOTSPOT : K1 : 1728 microseconds
ABCD : HOTSPOT : K1 : 1732 microseconds
ABCD : HOTSPOT : K1 : 1730 microseconds
ABCD : HOTSPOT : K1 : 1730 microseconds
ABCD : HOTSPOT : K1 : 1730 microseconds

Kernel time: 0.090 seconds
Total time: 0.090 seconds
ABCD : END


ABCD : WGSIZE : 64
ABCD : INPUT : 128
ABCD : NUM_I : 3
ABCD : ITERATION : 2
WG size of kernel = 64 X 64
Platform: Texas Instruments, Inc.
Device: TI Multicore C66 DSP
Starting kernels...Block_rows= 3 
Block_Cols= 3 
ABCD : HOTSPOT : K1 : 4265 microseconds
ABCD : HOTSPOT : K1 : 1770 microseconds
ABCD : HOTSPOT : K1 : 1750 microseconds
ABCD : HOTSPOT : K1 : 1739 microseconds
ABCD : HOTSPOT : K1 : 1737 microseconds
ABCD : HOTSPOT : K1 : 1730 microseconds
ABCD : HOTSPOT : K1 : 1736 microseconds
ABCD : HOTSPOT : K1 : 1730 microseconds
ABCD : HOTSPOT : K1 : 1749 microseconds
ABCD : HOTSPOT : K1 : 1730 microseconds
ABCD : HOTSPOT : K1 : 1740 microseconds
ABCD : HOTSPOT : K1 : 1730 microseconds
ABCD : HOTSPOT : K1 : 1728 microseconds
ABCD : HOTSPOT : K1 : 1731 microseconds
ABCD : HOTSPOT : K1 : 1734 microseconds
ABCD : HOTSPOT : K1 : 1731 microseconds
ABCD : HOTSPOT : K1 : 1730 microseconds
ABCD : HOTSPOT : K1 : 1727 microseconds
ABCD : HOTSPOT : K1 : 1735 microseconds
ABCD : HOTSPOT : K1 : 1730 microseconds
ABCD : HOTSPOT : K1 : 1731 microseconds
ABCD : HOTSPOT : K1 : 1728 microseconds
ABCD : HOTSPOT : K1 : 1733 microseconds
ABCD : HOTSPOT : K1 : 1731 microseconds
ABCD : HOTSPOT : K1 : 1728 microseconds
ABCD : HOTSPOT : K1 : 1731 microseconds
ABCD : HOTSPOT : K1 : 1732 microseconds
ABCD : HOTSPOT : K1 : 1734 microseconds
ABCD : HOTSPOT : K1 : 1735 microseconds
ABCD : HOTSPOT : K1 : 1727 microseconds
ABCD : HOTSPOT : K1 : 1743 microseconds
ABCD : HOTSPOT : K1 : 1736 microseconds
ABCD : HOTSPOT : K1 : 1736 microseconds
ABCD : HOTSPOT : K1 : 1729 microseconds
ABCD : HOTSPOT : K1 : 1730 microseconds
ABCD : HOTSPOT : K1 : 1727 microseconds
ABCD : HOTSPOT : K1 : 1731 microseconds
ABCD : HOTSPOT : K1 : 1730 microseconds
ABCD : HOTSPOT : K1 : 1727 microseconds
ABCD : HOTSPOT : K1 : 1733 microseconds
ABCD : HOTSPOT : K1 : 1728 microseconds
ABCD : HOTSPOT : K1 : 1726 microseconds
ABCD : HOTSPOT : K1 : 1733 microseconds
ABCD : HOTSPOT : K1 : 1734 microseconds
ABCD : HOTSPOT : K1 : 1728 microseconds
ABCD : HOTSPOT : K1 : 1730 microseconds
ABCD : HOTSPOT : K1 : 1729 microseconds
ABCD : HOTSPOT : K1 : 1728 microseconds
ABCD : HOTSPOT : K1 : 1732 microseconds
ABCD : HOTSPOT : K1 : 1727 microseconds

Kernel time: 0.090 seconds
Total time: 0.090 seconds
ABCD : END


ABCD : WGSIZE : 64
ABCD : INPUT : 128
ABCD : NUM_I : 3
ABCD : ITERATION : 3
WG size of kernel = 64 X 64
Platform: Texas Instruments, Inc.
Device: TI Multicore C66 DSP
Starting kernels...Block_rows= 3 
Block_Cols= 3 
ABCD : HOTSPOT : K1 : 4153 microseconds
ABCD : HOTSPOT : K1 : 1747 microseconds
ABCD : HOTSPOT : K1 : 1740 microseconds
ABCD : HOTSPOT : K1 : 1737 microseconds
ABCD : HOTSPOT : K1 : 1734 microseconds
ABCD : HOTSPOT : K1 : 1731 microseconds
ABCD : HOTSPOT : K1 : 1737 microseconds
ABCD : HOTSPOT : K1 : 1735 microseconds
ABCD : HOTSPOT : K1 : 1733 microseconds
ABCD : HOTSPOT : K1 : 1732 microseconds
ABCD : HOTSPOT : K1 : 1750 microseconds
ABCD : HOTSPOT : K1 : 1737 microseconds
ABCD : HOTSPOT : K1 : 1732 microseconds
ABCD : HOTSPOT : K1 : 1730 microseconds
ABCD : HOTSPOT : K1 : 1752 microseconds
ABCD : HOTSPOT : K1 : 1734 microseconds
ABCD : HOTSPOT : K1 : 1731 microseconds
ABCD : HOTSPOT : K1 : 1729 microseconds
ABCD : HOTSPOT : K1 : 1735 microseconds
ABCD : HOTSPOT : K1 : 1733 microseconds
ABCD : HOTSPOT : K1 : 1737 microseconds
ABCD : HOTSPOT : K1 : 1728 microseconds
ABCD : HOTSPOT : K1 : 1729 microseconds
ABCD : HOTSPOT : K1 : 1731 microseconds
ABCD : HOTSPOT : K1 : 1728 microseconds
ABCD : HOTSPOT : K1 : 1729 microseconds
ABCD : HOTSPOT : K1 : 1730 microseconds
ABCD : HOTSPOT : K1 : 1730 microseconds
ABCD : HOTSPOT : K1 : 1731 microseconds
ABCD : HOTSPOT : K1 : 1731 microseconds
ABCD : HOTSPOT : K1 : 1732 microseconds
ABCD : HOTSPOT : K1 : 1733 microseconds
ABCD : HOTSPOT : K1 : 1731 microseconds
ABCD : HOTSPOT : K1 : 1735 microseconds
ABCD : HOTSPOT : K1 : 1729 microseconds
ABCD : HOTSPOT : K1 : 1732 microseconds
ABCD : HOTSPOT : K1 : 1727 microseconds
ABCD : HOTSPOT : K1 : 1735 microseconds
ABCD : HOTSPOT : K1 : 1731 microseconds
ABCD : HOTSPOT : K1 : 1732 microseconds
ABCD : HOTSPOT : K1 : 1727 microseconds
ABCD : HOTSPOT : K1 : 1733 microseconds
ABCD : HOTSPOT : K1 : 1730 microseconds
ABCD : HOTSPOT : K1 : 1735 microseconds
ABCD : HOTSPOT : K1 : 1732 microseconds
ABCD : HOTSPOT : K1 : 1732 microseconds
ABCD : HOTSPOT : K1 : 1732 microseconds
ABCD : HOTSPOT : K1 : 1735 microseconds
ABCD : HOTSPOT : K1 : 1731 microseconds
ABCD : HOTSPOT : K1 : 1732 microseconds

Kernel time: 0.090 seconds
Total time: 0.090 seconds
ABCD : END


