~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Input: 1024
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

ABCD : WGSIZE : 32
ABCD : INPUT : 1024
ABCD : NUM_I : 3
ABCD : ITERATION : 1
WG size of kernel = 16 X 16
Platform: Texas Instruments, Inc.
Device: TI Multicore C66 DSP
Starting kernels...Block_rows= 86 
Block_Cols= 86 
ABCD : HOTSPOT : K1 : 68469 microseconds
ABCD : HOTSPOT : K1 : 49796 microseconds
ABCD : HOTSPOT : K1 : 49791 microseconds
ABCD : HOTSPOT : K1 : 49671 microseconds
ABCD : HOTSPOT : K1 : 49771 microseconds
ABCD : HOTSPOT : K1 : 49712 microseconds
ABCD : HOTSPOT : K1 : 49765 microseconds
ABCD : HOTSPOT : K1 : 49696 microseconds
ABCD : HOTSPOT : K1 : 49776 microseconds
ABCD : HOTSPOT : K1 : 49676 microseconds
ABCD : HOTSPOT : K1 : 49785 microseconds
ABCD : HOTSPOT : K1 : 49661 microseconds
ABCD : HOTSPOT : K1 : 49730 microseconds
ABCD : HOTSPOT : K1 : 49634 microseconds
ABCD : HOTSPOT : K1 : 49711 microseconds
ABCD : HOTSPOT : K1 : 49631 microseconds
ABCD : HOTSPOT : K1 : 49761 microseconds
ABCD : HOTSPOT : K1 : 49625 microseconds
ABCD : HOTSPOT : K1 : 49716 microseconds
ABCD : HOTSPOT : K1 : 49657 microseconds
ABCD : HOTSPOT : K1 : 49781 microseconds
ABCD : HOTSPOT : K1 : 49628 microseconds
ABCD : HOTSPOT : K1 : 49767 microseconds
ABCD : HOTSPOT : K1 : 49654 microseconds
ABCD : HOTSPOT : K1 : 49745 microseconds

Kernel time: 1.262 seconds
Total time: 1.263 seconds
ABCD : END


ABCD : WGSIZE : 32
ABCD : INPUT : 1024
ABCD : NUM_I : 3
ABCD : ITERATION : 2
WG size of kernel = 16 X 16
Platform: Texas Instruments, Inc.
Device: TI Multicore C66 DSP
Starting kernels...Block_rows= 86 
Block_Cols= 86 
ABCD : HOTSPOT : K1 : 67611 microseconds
ABCD : HOTSPOT : K1 : 49742 microseconds
ABCD : HOTSPOT : K1 : 49748 microseconds
ABCD : HOTSPOT : K1 : 49642 microseconds
ABCD : HOTSPOT : K1 : 49769 microseconds
ABCD : HOTSPOT : K1 : 49638 microseconds
ABCD : HOTSPOT : K1 : 49762 microseconds
ABCD : HOTSPOT : K1 : 49625 microseconds
ABCD : HOTSPOT : K1 : 49734 microseconds
ABCD : HOTSPOT : K1 : 49649 microseconds
ABCD : HOTSPOT : K1 : 49770 microseconds
ABCD : HOTSPOT : K1 : 49599 microseconds
ABCD : HOTSPOT : K1 : 49781 microseconds
ABCD : HOTSPOT : K1 : 49670 microseconds
ABCD : HOTSPOT : K1 : 49757 microseconds
ABCD : HOTSPOT : K1 : 49644 microseconds
ABCD : HOTSPOT : K1 : 49705 microseconds
ABCD : HOTSPOT : K1 : 49618 microseconds
ABCD : HOTSPOT : K1 : 49733 microseconds
ABCD : HOTSPOT : K1 : 49594 microseconds
ABCD : HOTSPOT : K1 : 49732 microseconds
ABCD : HOTSPOT : K1 : 49622 microseconds
ABCD : HOTSPOT : K1 : 49724 microseconds
ABCD : HOTSPOT : K1 : 49635 microseconds
ABCD : HOTSPOT : K1 : 49742 microseconds

Kernel time: 1.261 seconds
Total time: 1.261 seconds
ABCD : END


ABCD : WGSIZE : 32
ABCD : INPUT : 1024
ABCD : NUM_I : 3
ABCD : ITERATION : 3
WG size of kernel = 16 X 16
Platform: Texas Instruments, Inc.
Device: TI Multicore C66 DSP
Starting kernels...Block_rows= 86 
Block_Cols= 86 
ABCD : HOTSPOT : K1 : 67513 microseconds
ABCD : HOTSPOT : K1 : 49775 microseconds
ABCD : HOTSPOT : K1 : 49800 microseconds
ABCD : HOTSPOT : K1 : 49708 microseconds
ABCD : HOTSPOT : K1 : 49806 microseconds
ABCD : HOTSPOT : K1 : 49694 microseconds
ABCD : HOTSPOT : K1 : 49829 microseconds
ABCD : HOTSPOT : K1 : 49685 microseconds
ABCD : HOTSPOT : K1 : 49813 microseconds
ABCD : HOTSPOT : K1 : 49692 microseconds
ABCD : HOTSPOT : K1 : 49802 microseconds
ABCD : HOTSPOT : K1 : 49648 microseconds
ABCD : HOTSPOT : K1 : 49759 microseconds
ABCD : HOTSPOT : K1 : 49658 microseconds
ABCD : HOTSPOT : K1 : 49780 microseconds
ABCD : HOTSPOT : K1 : 49641 microseconds
ABCD : HOTSPOT : K1 : 49779 microseconds
ABCD : HOTSPOT : K1 : 49652 microseconds
ABCD : HOTSPOT : K1 : 49772 microseconds
ABCD : HOTSPOT : K1 : 49671 microseconds
ABCD : HOTSPOT : K1 : 49772 microseconds
ABCD : HOTSPOT : K1 : 49687 microseconds
ABCD : HOTSPOT : K1 : 49797 microseconds
ABCD : HOTSPOT : K1 : 49695 microseconds
ABCD : HOTSPOT : K1 : 49827 microseconds

Kernel time: 1.262 seconds
Total time: 1.262 seconds
ABCD : END


