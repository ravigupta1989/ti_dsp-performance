~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Input: 64
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

ABCD : WGSIZE : 64
ABCD : INPUT : 64
ABCD : NUM_I : 3
ABCD : ITERATION : 1
WG size of kernel = 64 X 64
Platform: Texas Instruments, Inc.
Device: TI Multicore C66 DSP
Starting kernels...Block_rows= 2 
Block_Cols= 2 
ABCD : HOTSPOT : K1 : 3704 microseconds
ABCD : HOTSPOT : K1 : 1536 microseconds
ABCD : HOTSPOT : K1 : 1526 microseconds
ABCD : HOTSPOT : K1 : 1520 microseconds
ABCD : HOTSPOT : K1 : 1526 microseconds
ABCD : HOTSPOT : K1 : 1521 microseconds
ABCD : HOTSPOT : K1 : 1520 microseconds
ABCD : HOTSPOT : K1 : 1517 microseconds
ABCD : HOTSPOT : K1 : 1521 microseconds
ABCD : HOTSPOT : K1 : 1517 microseconds
ABCD : HOTSPOT : K1 : 1532 microseconds
ABCD : HOTSPOT : K1 : 1519 microseconds
ABCD : HOTSPOT : K1 : 1522 microseconds
ABCD : HOTSPOT : K1 : 1517 microseconds
ABCD : HOTSPOT : K1 : 1522 microseconds
ABCD : HOTSPOT : K1 : 1515 microseconds
ABCD : HOTSPOT : K1 : 1522 microseconds
ABCD : HOTSPOT : K1 : 1516 microseconds
ABCD : HOTSPOT : K1 : 1521 microseconds
ABCD : HOTSPOT : K1 : 1517 microseconds
ABCD : HOTSPOT : K1 : 1520 microseconds
ABCD : HOTSPOT : K1 : 1516 microseconds
ABCD : HOTSPOT : K1 : 1517 microseconds
ABCD : HOTSPOT : K1 : 1520 microseconds
ABCD : HOTSPOT : K1 : 1518 microseconds
ABCD : HOTSPOT : K1 : 1519 microseconds
ABCD : HOTSPOT : K1 : 1516 microseconds
ABCD : HOTSPOT : K1 : 1516 microseconds
ABCD : HOTSPOT : K1 : 1517 microseconds
ABCD : HOTSPOT : K1 : 1516 microseconds
ABCD : HOTSPOT : K1 : 1519 microseconds
ABCD : HOTSPOT : K1 : 1521 microseconds
ABCD : HOTSPOT : K1 : 1518 microseconds
ABCD : HOTSPOT : K1 : 1516 microseconds
ABCD : HOTSPOT : K1 : 1517 microseconds
ABCD : HOTSPOT : K1 : 1516 microseconds
ABCD : HOTSPOT : K1 : 1522 microseconds
ABCD : HOTSPOT : K1 : 1518 microseconds
ABCD : HOTSPOT : K1 : 1516 microseconds
ABCD : HOTSPOT : K1 : 1515 microseconds
ABCD : HOTSPOT : K1 : 1517 microseconds
ABCD : HOTSPOT : K1 : 1515 microseconds
ABCD : HOTSPOT : K1 : 1516 microseconds
ABCD : HOTSPOT : K1 : 1521 microseconds
ABCD : HOTSPOT : K1 : 1518 microseconds
ABCD : HOTSPOT : K1 : 1519 microseconds
ABCD : HOTSPOT : K1 : 1520 microseconds
ABCD : HOTSPOT : K1 : 1517 microseconds
ABCD : HOTSPOT : K1 : 1517 microseconds
ABCD : HOTSPOT : K1 : 1516 microseconds

Kernel time: 0.079 seconds
Total time: 0.079 seconds
ABCD : END


ABCD : WGSIZE : 64
ABCD : INPUT : 64
ABCD : NUM_I : 3
ABCD : ITERATION : 2
WG size of kernel = 64 X 64
Platform: Texas Instruments, Inc.
Device: TI Multicore C66 DSP
Starting kernels...Block_rows= 2 
Block_Cols= 2 
ABCD : HOTSPOT : K1 : 3701 microseconds
ABCD : HOTSPOT : K1 : 1534 microseconds
ABCD : HOTSPOT : K1 : 1526 microseconds
ABCD : HOTSPOT : K1 : 1520 microseconds
ABCD : HOTSPOT : K1 : 1522 microseconds
ABCD : HOTSPOT : K1 : 1519 microseconds
ABCD : HOTSPOT : K1 : 1523 microseconds
ABCD : HOTSPOT : K1 : 1518 microseconds
ABCD : HOTSPOT : K1 : 1523 microseconds
ABCD : HOTSPOT : K1 : 1515 microseconds
ABCD : HOTSPOT : K1 : 1531 microseconds
ABCD : HOTSPOT : K1 : 1517 microseconds
ABCD : HOTSPOT : K1 : 1520 microseconds
ABCD : HOTSPOT : K1 : 1518 microseconds
ABCD : HOTSPOT : K1 : 1518 microseconds
ABCD : HOTSPOT : K1 : 1517 microseconds
ABCD : HOTSPOT : K1 : 1517 microseconds
ABCD : HOTSPOT : K1 : 1515 microseconds
ABCD : HOTSPOT : K1 : 1519 microseconds
ABCD : HOTSPOT : K1 : 1521 microseconds
ABCD : HOTSPOT : K1 : 1520 microseconds
ABCD : HOTSPOT : K1 : 1519 microseconds
ABCD : HOTSPOT : K1 : 1516 microseconds
ABCD : HOTSPOT : K1 : 1517 microseconds
ABCD : HOTSPOT : K1 : 1516 microseconds
ABCD : HOTSPOT : K1 : 1523 microseconds
ABCD : HOTSPOT : K1 : 1521 microseconds
ABCD : HOTSPOT : K1 : 1516 microseconds
ABCD : HOTSPOT : K1 : 1517 microseconds
ABCD : HOTSPOT : K1 : 1518 microseconds
ABCD : HOTSPOT : K1 : 1516 microseconds
ABCD : HOTSPOT : K1 : 1522 microseconds
ABCD : HOTSPOT : K1 : 1519 microseconds
ABCD : HOTSPOT : K1 : 1515 microseconds
ABCD : HOTSPOT : K1 : 1516 microseconds
ABCD : HOTSPOT : K1 : 1517 microseconds
ABCD : HOTSPOT : K1 : 1518 microseconds
ABCD : HOTSPOT : K1 : 1514 microseconds
ABCD : HOTSPOT : K1 : 1520 microseconds
ABCD : HOTSPOT : K1 : 1520 microseconds
ABCD : HOTSPOT : K1 : 1519 microseconds
ABCD : HOTSPOT : K1 : 1516 microseconds
ABCD : HOTSPOT : K1 : 1519 microseconds
ABCD : HOTSPOT : K1 : 1516 microseconds
ABCD : HOTSPOT : K1 : 1521 microseconds
ABCD : HOTSPOT : K1 : 1519 microseconds
ABCD : HOTSPOT : K1 : 1516 microseconds
ABCD : HOTSPOT : K1 : 1516 microseconds
ABCD : HOTSPOT : K1 : 1516 microseconds
ABCD : HOTSPOT : K1 : 1513 microseconds

Kernel time: 0.079 seconds
Total time: 0.079 seconds
ABCD : END


ABCD : WGSIZE : 64
ABCD : INPUT : 64
ABCD : NUM_I : 3
ABCD : ITERATION : 3
WG size of kernel = 64 X 64
Platform: Texas Instruments, Inc.
Device: TI Multicore C66 DSP
Starting kernels...Block_rows= 2 
Block_Cols= 2 
ABCD : HOTSPOT : K1 : 3672 microseconds
ABCD : HOTSPOT : K1 : 1535 microseconds
ABCD : HOTSPOT : K1 : 1524 microseconds
ABCD : HOTSPOT : K1 : 1525 microseconds
ABCD : HOTSPOT : K1 : 1526 microseconds
ABCD : HOTSPOT : K1 : 1520 microseconds
ABCD : HOTSPOT : K1 : 1520 microseconds
ABCD : HOTSPOT : K1 : 1518 microseconds
ABCD : HOTSPOT : K1 : 1522 microseconds
ABCD : HOTSPOT : K1 : 1516 microseconds
ABCD : HOTSPOT : K1 : 1528 microseconds
ABCD : HOTSPOT : K1 : 1523 microseconds
ABCD : HOTSPOT : K1 : 1522 microseconds
ABCD : HOTSPOT : K1 : 1515 microseconds
ABCD : HOTSPOT : K1 : 1522 microseconds
ABCD : HOTSPOT : K1 : 1517 microseconds
ABCD : HOTSPOT : K1 : 1519 microseconds
ABCD : HOTSPOT : K1 : 1519 microseconds
ABCD : HOTSPOT : K1 : 1521 microseconds
ABCD : HOTSPOT : K1 : 1515 microseconds
ABCD : HOTSPOT : K1 : 1520 microseconds
ABCD : HOTSPOT : K1 : 1515 microseconds
ABCD : HOTSPOT : K1 : 1517 microseconds
ABCD : HOTSPOT : K1 : 1517 microseconds
ABCD : HOTSPOT : K1 : 1518 microseconds
ABCD : HOTSPOT : K1 : 1519 microseconds
ABCD : HOTSPOT : K1 : 1518 microseconds
ABCD : HOTSPOT : K1 : 1517 microseconds
ABCD : HOTSPOT : K1 : 1518 microseconds
ABCD : HOTSPOT : K1 : 1516 microseconds
ABCD : HOTSPOT : K1 : 1519 microseconds
ABCD : HOTSPOT : K1 : 1519 microseconds
ABCD : HOTSPOT : K1 : 1518 microseconds
ABCD : HOTSPOT : K1 : 1517 microseconds
ABCD : HOTSPOT : K1 : 1515 microseconds
ABCD : HOTSPOT : K1 : 1521 microseconds
ABCD : HOTSPOT : K1 : 1515 microseconds
ABCD : HOTSPOT : K1 : 1519 microseconds
ABCD : HOTSPOT : K1 : 1516 microseconds
ABCD : HOTSPOT : K1 : 1517 microseconds
ABCD : HOTSPOT : K1 : 1517 microseconds
ABCD : HOTSPOT : K1 : 1516 microseconds
ABCD : HOTSPOT : K1 : 1516 microseconds
ABCD : HOTSPOT : K1 : 1517 microseconds
ABCD : HOTSPOT : K1 : 1519 microseconds
ABCD : HOTSPOT : K1 : 1516 microseconds
ABCD : HOTSPOT : K1 : 1519 microseconds
ABCD : HOTSPOT : K1 : 1518 microseconds
ABCD : HOTSPOT : K1 : 1519 microseconds
ABCD : HOTSPOT : K1 : 1515 microseconds

Kernel time: 0.079 seconds
Total time: 0.079 seconds
ABCD : END


