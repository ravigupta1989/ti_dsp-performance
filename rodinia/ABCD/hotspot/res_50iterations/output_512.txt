~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Input: 512
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

ABCD : WGSIZE : 256
ABCD : INPUT : 512
ABCD : NUM_I : 3
ABCD : ITERATION : 1
WG size of kernel = 256 X 256
Platform: Texas Instruments, Inc.
Device: TI Multicore C66 DSP
Starting kernels...Block_rows= 3 
Block_Cols= 3 
ABCD : HOTSPOT : K1 : 665280 microseconds
ABCD : HOTSPOT : K1 : 612385 microseconds
ABCD : HOTSPOT : K1 : 633132 microseconds
ABCD : HOTSPOT : K1 : 631023 microseconds
ABCD : HOTSPOT : K1 : 632260 microseconds
ABCD : HOTSPOT : K1 : 633632 microseconds
ABCD : HOTSPOT : K1 : 630540 microseconds
ABCD : HOTSPOT : K1 : 632830 microseconds
ABCD : HOTSPOT : K1 : 630195 microseconds
ABCD : HOTSPOT : K1 : 635394 microseconds
ABCD : HOTSPOT : K1 : 633224 microseconds
ABCD : HOTSPOT : K1 : 632536 microseconds
ABCD : HOTSPOT : K1 : 631834 microseconds
ABCD : HOTSPOT : K1 : 632648 microseconds
ABCD : HOTSPOT : K1 : 634473 microseconds
ABCD : HOTSPOT : K1 : 634206 microseconds
ABCD : HOTSPOT : K1 : 631651 microseconds
ABCD : HOTSPOT : K1 : 637679 microseconds
ABCD : HOTSPOT : K1 : 634755 microseconds
ABCD : HOTSPOT : K1 : 629970 microseconds
ABCD : HOTSPOT : K1 : 635519 microseconds
ABCD : HOTSPOT : K1 : 628831 microseconds
ABCD : HOTSPOT : K1 : 633193 microseconds
ABCD : HOTSPOT : K1 : 635787 microseconds
ABCD : HOTSPOT : K1 : 634201 microseconds

Kernel time: 15.838 seconds
Total time: 15.838 seconds
ABCD : END


ABCD : WGSIZE : 256
ABCD : INPUT : 512
ABCD : NUM_I : 3
ABCD : ITERATION : 2
WG size of kernel = 256 X 256
Platform: Texas Instruments, Inc.
Device: TI Multicore C66 DSP
Starting kernels...Block_rows= 3 
Block_Cols= 3 
ABCD : HOTSPOT : K1 : 661422 microseconds
ABCD : HOTSPOT : K1 : 635476 microseconds
ABCD : HOTSPOT : K1 : 634247 microseconds
ABCD : HOTSPOT : K1 : 599292 microseconds
ABCD : HOTSPOT : K1 : 631894 microseconds
ABCD : HOTSPOT : K1 : 636711 microseconds
ABCD : HOTSPOT : K1 : 629503 microseconds
ABCD : HOTSPOT : K1 : 633364 microseconds
ABCD : HOTSPOT : K1 : 627224 microseconds
ABCD : HOTSPOT : K1 : 634492 microseconds
ABCD : HOTSPOT : K1 : 633656 microseconds
ABCD : HOTSPOT : K1 : 633359 microseconds
ABCD : HOTSPOT : K1 : 635594 microseconds
ABCD : HOTSPOT : K1 : 635702 microseconds
ABCD : HOTSPOT : K1 : 635858 microseconds
ABCD : HOTSPOT : K1 : 631815 microseconds
ABCD : HOTSPOT : K1 : 607320 microseconds
ABCD : HOTSPOT : K1 : 632259 microseconds
ABCD : HOTSPOT : K1 : 630071 microseconds
ABCD : HOTSPOT : K1 : 634139 microseconds
ABCD : HOTSPOT : K1 : 632801 microseconds
ABCD : HOTSPOT : K1 : 641230 microseconds
ABCD : HOTSPOT : K1 : 624900 microseconds
ABCD : HOTSPOT : K1 : 635607 microseconds
ABCD : HOTSPOT : K1 : 631407 microseconds

Kernel time: 15.800 seconds
Total time: 15.800 seconds
ABCD : END


ABCD : WGSIZE : 256
ABCD : INPUT : 512
ABCD : NUM_I : 3
ABCD : ITERATION : 3
WG size of kernel = 256 X 256
Platform: Texas Instruments, Inc.
Device: TI Multicore C66 DSP
Starting kernels...Block_rows= 3 
Block_Cols= 3 
ABCD : HOTSPOT : K1 : 657057 microseconds
ABCD : HOTSPOT : K1 : 639938 microseconds
ABCD : HOTSPOT : K1 : 638424 microseconds
ABCD : HOTSPOT : K1 : 631988 microseconds
ABCD : HOTSPOT : K1 : 629680 microseconds
ABCD : HOTSPOT : K1 : 632524 microseconds
ABCD : HOTSPOT : K1 : 640654 microseconds
ABCD : HOTSPOT : K1 : 631791 microseconds
ABCD : HOTSPOT : K1 : 631770 microseconds
ABCD : HOTSPOT : K1 : 637638 microseconds
ABCD : HOTSPOT : K1 : 639837 microseconds
ABCD : HOTSPOT : K1 : 635386 microseconds
ABCD : HOTSPOT : K1 : 632122 microseconds
ABCD : HOTSPOT : K1 : 634681 microseconds
ABCD : HOTSPOT : K1 : 626103 microseconds
ABCD : HOTSPOT : K1 : 636268 microseconds
ABCD : HOTSPOT : K1 : 632235 microseconds
ABCD : HOTSPOT : K1 : 630509 microseconds
ABCD : HOTSPOT : K1 : 634231 microseconds
ABCD : HOTSPOT : K1 : 637901 microseconds
ABCD : HOTSPOT : K1 : 632548 microseconds
ABCD : HOTSPOT : K1 : 632041 microseconds
ABCD : HOTSPOT : K1 : 632498 microseconds
ABCD : HOTSPOT : K1 : 639557 microseconds
ABCD : HOTSPOT : K1 : 635496 microseconds

Kernel time: 15.883 seconds
Total time: 15.884 seconds
ABCD : END


