rm -f hotspot
gcc -DRD_WG_SIZE_0=32 -g -O3 -Wall -o hotspot -I/usr/include/CL OpenCL_helper_library.c hotspot.c -L/usr/include/CL -lOpenCL
WG size of kernel = 32 X 32
Platform: Texas Instruments, Inc.
Device: TI Multicore C66 DSP
Starting kernels...Block_rows= 37 
Block_Cols= 37 
Elapsed time: 1057440 microseconds

Kernel time: 1.057 seconds
Total time: 1.058 seconds
rm -f hotspot
gcc -DRD_WG_SIZE_0=64 -g -O3 -Wall -o hotspot -I/usr/include/CL OpenCL_helper_library.c hotspot.c -L/usr/include/CL -lOpenCL
WG size of kernel = 64 X 64
Platform: Texas Instruments, Inc.
Device: TI Multicore C66 DSP
Starting kernels...Block_rows= 18 
Block_Cols= 18 
Elapsed time: 1360379 microseconds

Kernel time: 1.360 seconds
Total time: 1.361 seconds
rm -f hotspot
gcc -DRD_WG_SIZE_0=96 -g -O3 -Wall -o hotspot -I/usr/include/CL OpenCL_helper_library.c hotspot.c -L/usr/include/CL -lOpenCL
WG size of kernel = 96 X 96
Platform: Texas Instruments, Inc.
Device: TI Multicore C66 DSP
Starting kernels...Block_rows= 12 
Block_Cols= 12 
Elapsed time: 1053744 microseconds

Kernel time: 1.054 seconds
Total time: 1.054 seconds
rm -f hotspot
gcc -DRD_WG_SIZE_0=128 -g -O3 -Wall -o hotspot -I/usr/include/CL OpenCL_helper_library.c hotspot.c -L/usr/include/CL -lOpenCL
WG size of kernel = 128 X 128
Platform: Texas Instruments, Inc.
Device: TI Multicore C66 DSP
Starting kernels...Block_rows= 9 
Block_Cols= 9 
Elapsed time: 25272939 microseconds

Kernel time: 25.273 seconds
Total time: 25.273 seconds
rm -f hotspot
gcc -DRD_WG_SIZE_0=256 -g -O3 -Wall -o hotspot -I/usr/include/CL OpenCL_helper_library.c hotspot.c -L/usr/include/CL -lOpenCL
WG size of kernel = 256 X 256
Platform: Texas Instruments, Inc.
Device: TI Multicore C66 DSP
Starting kernels...Block_rows= 5 
Block_Cols= 5 
Elapsed time: 33741503 microseconds

Kernel time: 33.742 seconds
Total time: 33.742 seconds
rm -f hotspot
gcc -DRD_WG_SIZE_0=512 -g -O3 -Wall -o hotspot -I/usr/include/CL OpenCL_helper_library.c hotspot.c -L/usr/include/CL -lOpenCL
WG size of kernel = 512 X 512
Platform: Texas Instruments, Inc.
Device: TI Multicore C66 DSP

>> Compilation failure
