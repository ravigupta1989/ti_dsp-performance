~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Input 1: 64
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Iteration: 1

WG size of kernel = 16 X 16
Platform: Texas Instruments, Inc.
Device: TI K2H DSP (8x C66)
Starting kernels...Block_rows= 6 
Block_Cols= 6 
Elapsed time: 1103 microseconds

Kernel time: 0.001 seconds
Total time: 0.001 seconds


Iteration: 2

WG size of kernel = 16 X 16
Platform: Texas Instruments, Inc.
Device: TI K2H DSP (8x C66)
Starting kernels...Block_rows= 6 
Block_Cols= 6 
Elapsed time: 1132 microseconds

Kernel time: 0.001 seconds
Total time: 0.001 seconds


Iteration: 3

WG size of kernel = 16 X 16
Platform: Texas Instruments, Inc.
Device: TI K2H DSP (8x C66)
Starting kernels...Block_rows= 6 
Block_Cols= 6 
Elapsed time: 1120 microseconds

Kernel time: 0.001 seconds
Total time: 0.001 seconds


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Input 2: 512
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Iteration: 1

WG size of kernel = 16 X 16
Platform: Texas Instruments, Inc.
Device: TI K2H DSP (8x C66)
Starting kernels...Block_rows= 43 
Block_Cols= 43 
Elapsed time: 19699 microseconds

Kernel time: 0.020 seconds
Total time: 0.020 seconds


Iteration: 2

WG size of kernel = 16 X 16
Platform: Texas Instruments, Inc.
Device: TI K2H DSP (8x C66)
Starting kernels...Block_rows= 43 
Block_Cols= 43 
Elapsed time: 19592 microseconds

Kernel time: 0.020 seconds
Total time: 0.020 seconds


Iteration: 3

WG size of kernel = 16 X 16
Platform: Texas Instruments, Inc.
Device: TI K2H DSP (8x C66)
Starting kernels...Block_rows= 43 
Block_Cols= 43 
Elapsed time: 19630 microseconds

Kernel time: 0.020 seconds
Total time: 0.020 seconds


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Input 3: 1024
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Iteration: 1

WG size of kernel = 16 X 16
Platform: Texas Instruments, Inc.
Device: TI K2H DSP (8x C66)
Starting kernels...Block_rows= 86 
Block_Cols= 86 
Elapsed time: 74894 microseconds

Kernel time: 0.075 seconds
Total time: 0.075 seconds


Iteration: 2

WG size of kernel = 16 X 16
Platform: Texas Instruments, Inc.
Device: TI K2H DSP (8x C66)
Starting kernels...Block_rows= 86 
Block_Cols= 86 
Elapsed time: 74651 microseconds

Kernel time: 0.075 seconds
Total time: 0.075 seconds


Iteration: 3

WG size of kernel = 16 X 16
Platform: Texas Instruments, Inc.
Device: TI K2H DSP (8x C66)
Starting kernels...Block_rows= 86 
Block_Cols= 86 
Elapsed time: 75245 microseconds

Kernel time: 0.075 seconds
Total time: 0.076 seconds


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Input 1: 64
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Iteration: 1

WG size of kernel = 32 X 32
Platform: Texas Instruments, Inc.
Device: TI K2H DSP (8x C66)
Starting kernels...Block_rows= 3 
Block_Cols= 3 
Elapsed time: 1402 microseconds

Kernel time: 0.001 seconds
Total time: 0.002 seconds


Iteration: 2

WG size of kernel = 32 X 32
Platform: Texas Instruments, Inc.
Device: TI K2H DSP (8x C66)
Starting kernels...Block_rows= 3 
Block_Cols= 3 
Elapsed time: 1370 microseconds

Kernel time: 0.001 seconds
Total time: 0.002 seconds


Iteration: 3

WG size of kernel = 32 X 32
Platform: Texas Instruments, Inc.
Device: TI K2H DSP (8x C66)
Starting kernels...Block_rows= 3 
Block_Cols= 3 
Elapsed time: 1383 microseconds

Kernel time: 0.001 seconds
Total time: 0.002 seconds


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Input 2: 512
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Iteration: 1

WG size of kernel = 32 X 32
Platform: Texas Instruments, Inc.
Device: TI K2H DSP (8x C66)
Starting kernels...Block_rows= 19 
Block_Cols= 19 
Elapsed time: 17233 microseconds

Kernel time: 0.017 seconds
Total time: 0.018 seconds


Iteration: 2

WG size of kernel = 32 X 32
Platform: Texas Instruments, Inc.
Device: TI K2H DSP (8x C66)
Starting kernels...Block_rows= 19 
Block_Cols= 19 
Elapsed time: 17043 microseconds

Kernel time: 0.017 seconds
Total time: 0.017 seconds


Iteration: 3

WG size of kernel = 32 X 32
Platform: Texas Instruments, Inc.
Device: TI K2H DSP (8x C66)
Starting kernels...Block_rows= 19 
Block_Cols= 19 
Elapsed time: 17234 microseconds

Kernel time: 0.017 seconds
Total time: 0.018 seconds


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Input 3: 1024
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Iteration: 1

WG size of kernel = 32 X 32
Platform: Texas Instruments, Inc.
Device: TI K2H DSP (8x C66)
Starting kernels...Block_rows= 37 
Block_Cols= 37 
Elapsed time: 60283 microseconds

Kernel time: 0.060 seconds
Total time: 0.061 seconds


Iteration: 2

WG size of kernel = 32 X 32
Platform: Texas Instruments, Inc.
Device: TI K2H DSP (8x C66)
Starting kernels...Block_rows= 37 
Block_Cols= 37 
Elapsed time: 60830 microseconds

Kernel time: 0.061 seconds
Total time: 0.061 seconds


Iteration: 3

WG size of kernel = 32 X 32
Platform: Texas Instruments, Inc.
Device: TI K2H DSP (8x C66)
Starting kernels...Block_rows= 37 
Block_Cols= 37 
Elapsed time: 60651 microseconds

Kernel time: 0.061 seconds
Total time: 0.061 seconds


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Input 1: 64
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Iteration: 1

WG size of kernel = 64 X 64
Platform: Texas Instruments, Inc.
Device: TI K2H DSP (8x C66)
Starting kernels...Block_rows= 2 
Block_Cols= 2 
Elapsed time: 3485 microseconds

Kernel time: 0.004 seconds
Total time: 0.004 seconds


Iteration: 2

WG size of kernel = 64 X 64
Platform: Texas Instruments, Inc.
Device: TI K2H DSP (8x C66)
Starting kernels...Block_rows= 2 
Block_Cols= 2 
Elapsed time: 3446 microseconds

Kernel time: 0.003 seconds
Total time: 0.004 seconds


Iteration: 3

WG size of kernel = 64 X 64
Platform: Texas Instruments, Inc.
Device: TI K2H DSP (8x C66)
Starting kernels...Block_rows= 2 
Block_Cols= 2 
Elapsed time: 3439 microseconds

Kernel time: 0.003 seconds
Total time: 0.004 seconds


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Input 2: 512
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Iteration: 1

WG size of kernel = 64 X 64
Platform: Texas Instruments, Inc.
Device: TI K2H DSP (8x C66)
Starting kernels...Block_rows= 9 
Block_Cols= 9 
Elapsed time: 21697 microseconds

Kernel time: 0.022 seconds
Total time: 0.022 seconds


Iteration: 2

WG size of kernel = 64 X 64
Platform: Texas Instruments, Inc.
Device: TI K2H DSP (8x C66)
Starting kernels...Block_rows= 9 
Block_Cols= 9 
Elapsed time: 21535 microseconds

Kernel time: 0.022 seconds
Total time: 0.022 seconds


Iteration: 3

WG size of kernel = 64 X 64
Platform: Texas Instruments, Inc.
Device: TI K2H DSP (8x C66)
Starting kernels...Block_rows= 9 
Block_Cols= 9 
Elapsed time: 21806 microseconds

Kernel time: 0.022 seconds
Total time: 0.022 seconds


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Input 3: 1024
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Iteration: 1

WG size of kernel = 64 X 64
Platform: Texas Instruments, Inc.
Device: TI K2H DSP (8x C66)
Starting kernels...Block_rows= 18 
Block_Cols= 18 
Elapsed time: 73759 microseconds

Kernel time: 0.074 seconds
Total time: 0.074 seconds


Iteration: 2

WG size of kernel = 64 X 64
Platform: Texas Instruments, Inc.
Device: TI K2H DSP (8x C66)
Starting kernels...Block_rows= 18 
Block_Cols= 18 
Elapsed time: 73782 microseconds

Kernel time: 0.074 seconds
Total time: 0.074 seconds


Iteration: 3

WG size of kernel = 64 X 64
Platform: Texas Instruments, Inc.
Device: TI K2H DSP (8x C66)
Starting kernels...Block_rows= 18 
Block_Cols= 18 
Elapsed time: 73558 microseconds

Kernel time: 0.074 seconds
Total time: 0.074 seconds


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Input 1: 64
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Iteration: 1

WG size of kernel = 128 X 128
Platform: Texas Instruments, Inc.
Device: TI K2H DSP (8x C66)
Starting kernels...Block_rows= 1 
Block_Cols= 1 
Elapsed time: 28618 microseconds

Kernel time: 0.029 seconds
Total time: 0.029 seconds


Iteration: 2

WG size of kernel = 128 X 128
Platform: Texas Instruments, Inc.
Device: TI K2H DSP (8x C66)
Starting kernels...Block_rows= 1 
Block_Cols= 1 
Elapsed time: 28670 microseconds

Kernel time: 0.029 seconds
Total time: 0.029 seconds


Iteration: 3

WG size of kernel = 128 X 128
Platform: Texas Instruments, Inc.
Device: TI K2H DSP (8x C66)
Starting kernels...Block_rows= 1 
Block_Cols= 1 
Elapsed time: 28711 microseconds

Kernel time: 0.029 seconds
Total time: 0.029 seconds


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Input 2: 512
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Iteration: 1

WG size of kernel = 128 X 128
Platform: Texas Instruments, Inc.
Device: TI K2H DSP (8x C66)
Starting kernels...Block_rows= 5 
Block_Cols= 5 
Elapsed time: 308621 microseconds

Kernel time: 0.309 seconds
Total time: 0.309 seconds


Iteration: 2

WG size of kernel = 128 X 128
Platform: Texas Instruments, Inc.
Device: TI K2H DSP (8x C66)
Starting kernels...Block_rows= 5 
Block_Cols= 5 
Elapsed time: 313453 microseconds

Kernel time: 0.313 seconds
Total time: 0.314 seconds


Iteration: 3

WG size of kernel = 128 X 128
Platform: Texas Instruments, Inc.
Device: TI K2H DSP (8x C66)
Starting kernels...Block_rows= 5 
Block_Cols= 5 
Elapsed time: 316420 microseconds

Kernel time: 0.316 seconds
Total time: 0.317 seconds


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Input 3: 1024
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Iteration: 1

WG size of kernel = 128 X 128
Platform: Texas Instruments, Inc.
Device: TI K2H DSP (8x C66)
Starting kernels...Block_rows= 9 
Block_Cols= 9 
Elapsed time: 1052794 microseconds

Kernel time: 1.053 seconds
Total time: 1.053 seconds


Iteration: 2

WG size of kernel = 128 X 128
Platform: Texas Instruments, Inc.
Device: TI K2H DSP (8x C66)
Starting kernels...Block_rows= 9 
Block_Cols= 9 
Elapsed time: 1045606 microseconds

Kernel time: 1.046 seconds
Total time: 1.046 seconds


Iteration: 3

WG size of kernel = 128 X 128
Platform: Texas Instruments, Inc.
Device: TI K2H DSP (8x C66)
Starting kernels...Block_rows= 9 
Block_Cols= 9 
Elapsed time: 1003673 microseconds

Kernel time: 1.004 seconds
Total time: 1.004 seconds


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Input 1: 64
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Iteration: 1

WG size of kernel = 256 X 256
Platform: Texas Instruments, Inc.
Device: TI K2H DSP (8x C66)
Starting kernels...Block_rows= 1 
Block_Cols= 1 
Elapsed time: 78136 microseconds

Kernel time: 0.078 seconds
Total time: 0.078 seconds


Iteration: 2

WG size of kernel = 256 X 256
Platform: Texas Instruments, Inc.
Device: TI K2H DSP (8x C66)
Starting kernels...Block_rows= 1 
Block_Cols= 1 
Elapsed time: 78145 microseconds

Kernel time: 0.078 seconds
Total time: 0.078 seconds


Iteration: 3

WG size of kernel = 256 X 256
Platform: Texas Instruments, Inc.
Device: TI K2H DSP (8x C66)
Starting kernels...Block_rows= 1 
Block_Cols= 1 
Elapsed time: 78393 microseconds

Kernel time: 0.078 seconds
Total time: 0.079 seconds


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Input 2: 512
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Iteration: 1

WG size of kernel = 256 X 256
Platform: Texas Instruments, Inc.
Device: TI K2H DSP (8x C66)
Starting kernels...Block_rows= 3 
Block_Cols= 3 
Elapsed time: 668059 microseconds

Kernel time: 0.668 seconds
Total time: 0.668 seconds


Iteration: 2

WG size of kernel = 256 X 256
Platform: Texas Instruments, Inc.
Device: TI K2H DSP (8x C66)
Starting kernels...Block_rows= 3 
Block_Cols= 3 
Elapsed time: 669069 microseconds

Kernel time: 0.669 seconds
Total time: 0.669 seconds


Iteration: 3

WG size of kernel = 256 X 256
Platform: Texas Instruments, Inc.
Device: TI K2H DSP (8x C66)
Starting kernels...Block_rows= 3 
Block_Cols= 3 
Elapsed time: 666705 microseconds

Kernel time: 0.667 seconds
Total time: 0.667 seconds


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Input 3: 1024
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Iteration: 1

WG size of kernel = 256 X 256
Platform: Texas Instruments, Inc.
Device: TI K2H DSP (8x C66)
Starting kernels...Block_rows= 5 
Block_Cols= 5 
Elapsed time: 1428462 microseconds

Kernel time: 1.428 seconds
Total time: 1.429 seconds


Iteration: 2

WG size of kernel = 256 X 256
Platform: Texas Instruments, Inc.
Device: TI K2H DSP (8x C66)
Starting kernels...Block_rows= 5 
Block_Cols= 5 
Elapsed time: 1454690 microseconds

Kernel time: 1.455 seconds
Total time: 1.455 seconds


Iteration: 3

WG size of kernel = 256 X 256
Platform: Texas Instruments, Inc.
Device: TI K2H DSP (8x C66)
Starting kernels...Block_rows= 5 
Block_Cols= 5 
Elapsed time: 1415241 microseconds

Kernel time: 1.415 seconds
Total time: 1.416 seconds


