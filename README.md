# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
This repository contains opencl application codes from rodinia that can run on HP Moonshot Server Nodes.

The Make file is already edited to run on HP Moonshot server codes.
The time stamps for each kernel code are collected and printed on the screen or can be redirected on a file. 

Sample run file. 
Each folder with application code contains a run file which has a sampple run command and Make command. It was necessary as each application uses diferent set of arguments. 

Varying the work group size:

The work group size can be changed by passing correct wg size to make command.
Each folder also contains a python file for repeated experiment which runs multiple times and takes an average of the timestamps. 

Last but not the least - Almost all the options follow from original Rodinia Code. Help files of them will be very helpful in running these codes.

Contact : Ravi Gupta : gupta237@purdue.edu