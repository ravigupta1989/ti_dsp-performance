~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Input: graph1048576.txt
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

ABCD : WGSIZE : 16384
ABCD : INPUT : graph1048576.txt
ABCD : NUM_I : 1
ABCD : ITERATION : 1
Thread weight = 1
Reading File
no_of_nodes 1048576 num_of_blocks 64 work_group_size 16384
Starting bfs on dsp...
Creating Buffers...
Buffers Created.
Setting values of buffers...
Done setting values.
Creating Kernels...
Kernels created.
Running Kernels...
ABCD : BFS : K1 : 808989 microseconds
ABCD : BFS : K2 : 707632 microseconds
ABCD : BFS : K1 : 740698 microseconds
ABCD : BFS : K2 : 707741 microseconds
ABCD : BFS : K1 : 740550 microseconds
ABCD : BFS : K2 : 707540 microseconds
ABCD : BFS : K1 : 740638 microseconds
ABCD : BFS : K2 : 707564 microseconds
ABCD : BFS : K1 : 740520 microseconds
ABCD : BFS : K2 : 707366 microseconds
ABCD : BFS : K1 : 742905 microseconds
ABCD : BFS : K2 : 707239 microseconds
ABCD : BFS : K1 : 755845 microseconds
ABCD : BFS : K2 : 711976 microseconds
ABCD : BFS : K1 : 810119 microseconds
ABCD : BFS : K2 : 725910 microseconds
ABCD : BFS : K1 : 855159 microseconds
ABCD : BFS : K2 : 718497 microseconds
ABCD : BFS : K1 : 777242 microseconds
ABCD : BFS : K2 : 707526 microseconds
ABCD : BFS : K1 : 739471 microseconds
ABCD : BFS : K2 : 707730 microseconds
ABCD : BFS : K1 : 740370 microseconds
ABCD : BFS : K2 : 707599 microseconds
Completed Kernels. #iterations=12
Done!
Finished running bfs on dsp.
Printing results...
Result stored in result.txt
ABCD : END


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Input: graph1048576.txt
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

ABCD : WGSIZE : 1
ABCD : INPUT : graph1048576.txt
ABCD : NUM_I : 1
ABCD : ITERATION : 1
Thread weight = 16384
Reading File
no_of_nodes 1048576 num_of_blocks 1048576 work_group_size 1
Starting bfs on dsp...
Creating Buffers...
Buffers Created.
Setting values of buffers...
Done setting values.
Creating Kernels...
Kernels created.
Running Kernels...
ABCD : BFS : K1 : 795929 microseconds
ABCD : BFS : K2 : 709870 microseconds
ABCD : BFS : K1 : 733716 microseconds
ABCD : BFS : K2 : 710160 microseconds
ABCD : BFS : K1 : 733277 microseconds
ABCD : BFS : K2 : 709974 microseconds
ABCD : BFS : K1 : 733260 microseconds
ABCD : BFS : K2 : 710064 microseconds
ABCD : BFS : K1 : 733666 microseconds
ABCD : BFS : K2 : 710392 microseconds
ABCD : BFS : K1 : 733197 microseconds
ABCD : BFS : K2 : 711698 microseconds
ABCD : BFS : K1 : 748058 microseconds
ABCD : BFS : K2 : 717580 microseconds
ABCD : BFS : K1 : 800420 microseconds
ABCD : BFS : K2 : 733540 microseconds
ABCD : BFS : K1 : 846324 microseconds
ABCD : BFS : K2 : 724765 microseconds
ABCD : BFS : K1 : 769553 microseconds
ABCD : BFS : K2 : 710582 microseconds
ABCD : BFS : K1 : 732599 microseconds
ABCD : BFS : K2 : 710169 microseconds
ABCD : BFS : K1 : 733358 microseconds
ABCD : BFS : K2 : 709989 microseconds
Completed Kernels. #iterations=12
Done!
Finished running bfs on dsp.
Printing results...
Result stored in result.txt
ABCD : END


