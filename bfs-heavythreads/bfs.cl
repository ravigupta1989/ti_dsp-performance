typedef struct
{
	int starting;
	int no_of_edges;
} Node;

#ifdef DEBUG
#define myprintf printf
#else
#define myprintf(arg...)
#endif

#ifndef THREAD_WEIGHT
	#define THREAD_WEIGHT 1
#endif

kernel void BFS_1( const global Node* g_graph_nodes,
                   const global int* g_graph_edges,
                   global bool* g_graph_mask,
                   global bool* g_updating_graph_mask,
                   global bool* g_graph_visited,
                   global int* g_cost,
                   const  int no_of_nodes,
		   global unsigned long int * g_timestamp)
{
	int groupid = get_group_id(0);
	int localid = get_local_id(0);		
	int tid_orig = get_global_id(0);
	int tid = THREAD_WEIGHT * tid_orig;
	int i = 0;
	
	if(groupid == 2)
	{
		g_timestamp[localid*2] = __clock64();
	}		


	for(i = 0; i < THREAD_WEIGHT; i++)
	{
		if( tid<no_of_nodes && g_graph_mask[tid])
		{
			g_graph_mask[tid]=false;
			for(int i=g_graph_nodes[tid].starting;
					i<(g_graph_nodes[tid].no_of_edges + g_graph_nodes[tid].starting);
					i++) {
				int id = g_graph_edges[i]; 
				if(!g_graph_visited[id])
				{
					g_cost[id]=g_cost[tid]+1;
					g_updating_graph_mask[id]=true;
				}
			}
		
		}
		__cache_l1d_flush();
		__cache_l2_flush();

		tid++;
	}


	if(groupid == 2)
	{
		g_timestamp[localid*2 + 1] = __clock64();
	}
}

kernel void BFS_2(global bool* g_graph_mask,
                  global bool* g_updating_graph_mask,
                   global bool* g_graph_visited,
                  global bool* g_over,
                  const  int no_of_nodes)
{
	int tid_orig = get_global_id(0);
	int tid = THREAD_WEIGHT*tid_orig;
	int i = 0;

	for(i = 0; i < THREAD_WEIGHT; i++)
	{
		if( tid<no_of_nodes && g_updating_graph_mask[tid])
		{
			g_graph_mask[tid]=true;
			g_graph_visited[tid]=true;
			*g_over=true;
			g_updating_graph_mask[tid]=false;
		}
		__cache_l1d_flush();
		__cache_l2_flush();

		tid++;
	}
}
















