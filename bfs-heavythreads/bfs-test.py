import subprocess
import sys
import os
import time

# Define these values:   ~~~~~~~~~~~~~~~~~~~~~~~~

input_data = ['graph1048576.txt']
num_iterations = 1;

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

if len(sys.argv) != 3 :
	print("Usage: python2 bfs-test.py <output_filename> \n\n")
	sys.exit()

size_list = sys.argv[2].split(",")

f = open(sys.argv[1], 'w')

for wgsize in size_list :
	os.system("make clean")
	thread_weight = str(16384 / int(wgsize))
	os.system("make KERNEL_DIM=\"-DBLOCK_SIZE=" + wgsize + "\" WEIGHT=\"-DTHREAD_WEIGHT=" + thread_weight + "\"")
	time.sleep(1)
	
	for data in input_data:
		f.write("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n")
		f.write("Input: " + data + "\n")
		f.write("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n")

		for i in range(1, num_iterations+1):
			result =  subprocess.check_output(['./bfs', data])
			f.write("ABCD : WGSIZE : " + wgsize + "\n")
			f.write("ABCD : INPUT : " + data + "\n")
			f.write("ABCD : NUM_I : " + str(num_iterations) + "\n")
			f.write("ABCD : ITERATION : " + str(i) + "\n")

			f.write(result)
		
			f.write("ABCD : END\n")
			f.write("\n\n")
                        time.sleep(1)












