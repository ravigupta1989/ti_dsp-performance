import subprocess
import sys
import os
import time

# Define these values:   ~~~~~~~~~~~~~~~~~~~~~~~~

input_data = ['./hotspot 1024 2 30 temp_1024 power_1024 output.out']
num_iterations = 1;

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

if len(sys.argv) != 3 :
	print("Usage: python2 cfd-test.py <output_filename> <list of wgsizes>\n\n")
	sys.exit()

size_list = sys.argv[2].split(",")

f = open(sys.argv[1], 'w')

for wgsize in size_list:
	os.system("make clean")
	compile_str = "make KERNEL_DIM=\"-DRD_WG_SIZE_0=1\" WEIGHT=\"-DTHREAD_WEIGHT=" + wgsize + "\""
	os.system(compile_str)
	time.sleep(1)


	for data in input_data :
		f.write("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n")
		f.write("Input: " + data + "\n")
		f.write("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n")

		for i in range(1, num_iterations+1):
			f.write("ABCD : WGSIZE : " + wgsize + "\n")
			f.write("ABCD : INPUT : " + data + "\n")
			f.write("ABCD : NUM_I : " + str(num_iterations) + "\n")
			f.write("ABCD : ITERATION : " + str(i) + "\n")
			result =  subprocess.check_output(data.split(' '))  #['./hotspot', '64', '2', '2', 'temp_64', 'power_64', 'output.out']
			time.sleep(1)
			
			f.write(result)

			f.write("ABCD : END\n")
			f.write("\n\n")














