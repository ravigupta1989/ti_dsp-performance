#define IN_RANGE(x, min, max)   ((x)>=(min) && (x)<=(max))

__kernel void hotspot(	int iteration,  //number of iteration
			global float *power,   //power input
			global float *temp_src,    //temperature input/output
			global float *temp_dst,    //temperature input/output
			int grid_cols,  //Col of grid
			int grid_rows,  //Row of grid
			int border_cols,  // border offset 
			int border_rows,  // border offset
			float Cap,      //Capacitance
			float Rx, 
			float Ry, 
			float Rz, 
			float step,
			__global unsigned long int * thread_start,
			__global unsigned long int * thread_end,
			__global unsigned long int * block_start,
			__global unsigned long int * block_end,
			__global int * core_num)
{
	local float temp_on_cuda[THREAD_WEIGHT][THREAD_WEIGHT];
	local float power_on_cuda[THREAD_WEIGHT][THREAD_WEIGHT];
	local float temp_t[THREAD_WEIGHT][THREAD_WEIGHT]; // saving temporary temperature result

	float amb_temp = 80.0f;
	float step_div_Cap;
	float Rx_1,Ry_1,Rz_1;

	int bx = get_group_id(0);
	int by = get_group_id(1);

	int tx = get_local_id(0);
	int ty = get_local_id(1);
	int gidx=get_global_id(0);
	int gidy=get_global_id(1);
	int localsize1 = get_local_size(0);
	int localsize2 = get_local_size(1);
	int num_blocks2 = get_global_size(1) / localsize2;

	if(bx == 2 && by == 2)
	{
		thread_start[localsize2 * ty + tx] = __clock64();
	}
	if(tx == 0 && ty == 0)
	{
		block_start[num_blocks2 * by + bx] = __clock64();
		core_num[num_blocks2 * by + bx] = __core_num();
	}

	step_div_Cap=step/Cap;

	Rx_1=1/Rx;
	Ry_1=1/Ry;
	Rz_1=1/Rz;



	// each block finally computes result for a small block
	// after N iterations. 
	// it is the non-overlapping small blocks that cover 
	// all the input data

	// calculate the small block size
	int small_block_rows = THREAD_WEIGHT-iteration*2;//EXPAND_RATE //const
	int small_block_cols = THREAD_WEIGHT-iteration*2;//EXPAND_RATE //const

	// calculate the boundary for the block according to 
	// the boundary of its small block
	int blkY = small_block_rows*by-border_rows;    //const 
	int blkX = small_block_cols*bx-border_cols;    //const
	int blkYmax = blkY+THREAD_WEIGHT-1;          //const
	int blkXmax = blkX+THREAD_WEIGHT-1;          //const

	// calculate the global thread coordination
	int yidx = blkY+ty;
	int xidx = blkX+tx;

	int loadYidx;int loadXidx;
	int index;
	int xqsy,yqsy;

	for (yqsy=0;yqsy<THREAD_WEIGHT;yqsy++)
	{
		for (xqsy=0;xqsy<THREAD_WEIGHT;xqsy++)
		{
			yidx = blkY+yqsy;
			xidx = blkX+xqsy;
			// load data if it is within the valid input range
			loadYidx=yidx;
			loadXidx=xidx;
			index = grid_cols*loadYidx+loadXidx;
			
			if(IN_RANGE(loadYidx, 0, grid_rows-1) && IN_RANGE(loadXidx, 0, grid_cols-1))
			{
				temp_on_cuda[yqsy][xqsy] = temp_src[index];  // Load the temperature data from global memory to shared memory
				power_on_cuda[yqsy][xqsy] = power[index];// Load the power data from global memory to shared memory
			}
     		}
   	}

	//barrier(CLK_LOCAL_MEM_FENCE);

	// effective range within this block that falls within 
	// the valid range of the input data
	// used to rule out computation outside the boundary.
	int validYmin = (blkY < 0) ? -blkY : 0; //const
	int validYmax = (blkYmax > grid_rows-1) ? THREAD_WEIGHT -1-(blkYmax-grid_rows+1) : THREAD_WEIGHT-1; //const
	int validXmin = (blkX < 0) ? -blkX : 0;   //const
	int validXmax = (blkXmax > grid_cols-1) ? THREAD_WEIGHT-1-(blkXmax-grid_cols+1) : THREAD_WEIGHT-1;  //const

	int N ;
	int S ;
	int W ;
	int E ;

	bool computed;
	for (int i=0; i<iteration ; i++)
	{
		for (yqsy=0;yqsy<THREAD_WEIGHT;yqsy++)
			{
				for (xqsy=0;xqsy<THREAD_WEIGHT;xqsy++)
				{
					N = yqsy-1;
					S = yqsy+1;
					W = xqsy-1;
					E = xqsy+1;
					N = (N < validYmin) ? validYmin : N;
					S = (S > validYmax) ? validYmax : S;
					W = (W < validXmin) ? validXmin : W;
					E = (E > validXmax) ? validXmax : E;

					if( IN_RANGE(xqsy, i+1, THREAD_WEIGHT-i-2) &&  \
					IN_RANGE(yqsy, i+1, THREAD_WEIGHT-i-2) &&  \
					IN_RANGE(xqsy, validXmin, validXmax) && \
					IN_RANGE(yqsy, validYmin, validYmax) )
					{
						temp_t[yqsy][xqsy] =   temp_on_cuda[yqsy][xqsy] + step_div_Cap * (power_on_cuda[yqsy][xqsy] + 
						(temp_on_cuda[S][xqsy] + temp_on_cuda[N][xqsy] - 2.0f * temp_on_cuda[yqsy][xqsy]) * Ry_1 + 
						(temp_on_cuda[yqsy][E] + temp_on_cuda[yqsy][W] - 2.0f * temp_on_cuda[yqsy][xqsy]) * Rx_1 + 
						(amb_temp - temp_on_cuda[yqsy][xqsy]) * Rz_1);
					}
						
				}
			}
			if(i==iteration-1)
			break;

			for (yqsy=0;yqsy<THREAD_WEIGHT;yqsy++)
			{
				for (xqsy=0;xqsy<THREAD_WEIGHT;xqsy++)
				{
					N = yqsy-1;
					S = yqsy+1;
					W = xqsy-1;
					E = xqsy+1;
					N = (N < validYmin) ? validYmin : N;
					S = (S > validYmax) ? validYmax : S;
					W = (W < validXmin) ? validXmin : W;
					E = (E > validXmax) ? validXmax : E;

					if( IN_RANGE(xqsy, i+1, THREAD_WEIGHT-i-2) &&  \
					IN_RANGE(yqsy, i+1, THREAD_WEIGHT-i-2) &&  \
					IN_RANGE(xqsy, validXmin, validXmax) && \
					IN_RANGE(yqsy, validYmin, validYmax) )
					{
						temp_on_cuda[yqsy][xqsy]= temp_t[yqsy][xqsy];
					}
				}
			}
  		 
		//barrier(CLK_LOCAL_MEM_FENCE);
					
		//barrier(CLK_LOCAL_MEM_FENCE);
	}

	// update the global memory
	// after the last iteration, only threads coordinated within the 
	// small block perform the calculation and switch on ``computed''

	//printf ("\n========\n");
	for (int i=0; i<iteration ; i++)
	{ 
		for (yqsy=0;yqsy<THREAD_WEIGHT;yqsy++)
		{
			for (xqsy=0;xqsy<THREAD_WEIGHT;xqsy++)
			{
				yidx = blkY+yqsy;
				xidx = blkX+xqsy;
				// load data if it is within the valid input range
				loadYidx=yidx;
				loadXidx=xidx;
				index = grid_cols*loadYidx+loadXidx;
				if(IN_RANGE(loadYidx, 0, grid_rows-1) && IN_RANGE(loadXidx, 0, grid_cols-1))
				{
					N = yqsy-1;
					S = yqsy+1;
					W = xqsy-1;
					E = xqsy+1;
					N = (N < validYmin) ? validYmin : N;
					S = (S > validYmax) ? validYmax : S;
					W = (W < validXmin) ? validXmin : W;
					E = (E > validXmax) ? validXmax : E;

					if( IN_RANGE(xqsy, i+1, THREAD_WEIGHT-i-2) &&  \
					IN_RANGE(yqsy, i+1, THREAD_WEIGHT-i-2) &&  \
					IN_RANGE(xqsy, validXmin, validXmax) && \
					IN_RANGE(yqsy, validYmin, validYmax) )
					{
						temp_dst[index]= temp_t[yqsy][xqsy];
					}

				}
			}	
		}
	}

	if((tx == localsize1-1) && (ty == localsize2-1))
	{
		block_end[num_blocks2 * by + bx] = __clock64();
	}
	if(bx == 2 && by == 2)
	{
		thread_end[localsize2 * ty + tx] = __clock64();
	}

}               















