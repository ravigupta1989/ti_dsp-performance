import subprocess
import sys
import os
import time
import string
import numpy

# the following values can be modified if necessary:
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

offload_count = 2 # number of offloads to collect data for
max_iterations = 20  # maximum number of iterations that the script can handle
max_kernels = 10  # maximum number of kernels (K1, K2, etc) that the script can handle

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


# initialize 2d matrix:
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
offload_times = []
num_offloads = []

thread_uptimes = []
thread_downtimes = []
num_threads = []

thread_uptime_arr = []
thread_downtime_arr = []

block_uptimes = []
block_downtimes = []
num_blocks = []

block_uptime_arr = []
block_downtime_arr = []

block_start = []
block_end = []

block_offload = 0
thread_offload = 0

for i in range(0, max_iterations) :
	offload_times.append([])
	num_offloads.append([])
	
	for j in range(0, max_kernels+1) :
		offload_times[i].append(0)
		num_offloads[i].append(0)

for i in range(0, max_kernels+1) :
	thread_uptimes.append(0)
	thread_downtimes.append(0)
	num_threads.append(0)
	
	thread_uptime_arr.append([])
	thread_downtime_arr.append([])

for i in range(0, 8) :
	block_start.append(0)
	block_end.append(0)
	
	block_uptimes.append([])
	block_downtimes.append([])
	num_blocks.append([])

	block_uptime_arr.append([])
	block_downtime_arr.append([])

	for j in range(0, max_kernels+1) :
		block_uptimes[i].append(0)
		block_downtimes[i].append(0)
		num_blocks[i].append(0)
		
		block_uptime_arr[i].append([])
		block_downtime_arr[i].append([])
	

# block_uptimes[core_num][kernel]


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#initialize stats matrices:
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
average_offload_time = []
average_offload_num = []
stdev_offload_time = []
stdev_offload_num = []

average_thread_time = []
average_thread_num = []
stdev_thread_time = []
stdev_thread_num = []

for i in range(0, max_kernels) :
	average_offload_time.append(0)
	average_offload_num.append(0)
	stdev_offload_time.append(0)
	stdev_offload_num.append(0)
	
	average_thread_time.append(0)
	average_thread_num.append(0)
	stdev_thread_time.append(0)
	stdev_thread_num.append(0)

thread_start = 0
thread_end = 0
block_start_temp = 0
block_end_temp = 0

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

if (len(sys.argv) != 2 and len(sys.argv) != 3 and len(sys.argv) != 4) or sys.argv[1] == '-h' or (len(sys.argv) == 4 and sys.argv[2] != '-c') :
	print("Usage: python2 getdata2.py <filename> [-h] [-a] [-t] [-b] [-c <core_num>]\n")
	print("Optional arguments: ")
	print("[-h]		display usage")
	print("[-a]		display application-level data")
	print("[-t]		display thread-level data")
	print("[-b]		display block-level data")
	print("[-c <core_num>]	select a core to display block-level data\n")
	print("NOTE: if none of these arguments are given, all data will be displayed\n")
	
	sys.exit()


filename = sys.argv[1];
f = open(filename, 'r')

#lines = f.read().splitlines()

line_number = 0

app_level = 0
thread_level = 0
block_level = 0
core_select = -1

if(len(sys.argv) == 3) :
	if sys.argv[2] == '-a' :
		app_level = 1
	elif sys.argv[2] == '-t' :
		thread_level = 1
	elif sys.argv[2] == '-b' :
		block_level = 1
	elif sys.argv[2] == 'c' :
		print "Select a core number"
		sys.exit()
elif(len(sys.argv) == 4) :
	if sys.argv[2] == '-c' :
		block_level = 1
		core_select = int(sys.argv[3])
else :
	app_level = 1
	thread_level = 1
	block_level = 1

temp_offload_time = []
temp_num_offloads = []
kernels = [' ', 'K1', 'K2', 'K3', 'K4', 'K5', 'K6', 'K7', 'K8', 'K9', 'K10']

for readLine in f :
	line_number += 1
	# Get offload Data
	if len(readLine) >= 4 and (readLine[0] == 'A' and readLine[1] == 'B' and readLine[2] == 'C' and readLine[3] == 'D') :
		terms = readLine.split( )
		if terms[2] == 'END' :
			offload_times[i-1].pop(0)
			num_offloads[i-1].pop(0)

			if iteration == str(num_iterations) :
				for j in range(0, 10) :
					for k in range(0, num_iterations) :
						temp_offload_time.append(offload_times[k][j])
						temp_num_offloads.append(num_offloads[k][j])

					average_offload_time[j] = sum(temp_offload_time) / len(temp_offload_time)
					average_offload_num[j] = sum(temp_num_offloads) / len(temp_num_offloads)
					
					stdev_offload_time[j] = numpy.std(temp_offload_time)
					stdev_offload_num[j] = numpy.std(temp_num_offloads)
				
					temp_offload_time = []
					temp_num_offloads = []
				
				if app_level == 1 :
					print("\n~~OFFLOAD DATA~~")
					print("WGSIZE = " + wgsize)
					print("Number of Iterations = " + str(num_iterations))
					print("Input data: " + data)
					print ', '.join(map(str, kernels))
					sys.stdout.write("Total Offload Time, ")
					print ', '.join(map(str, average_offload_time))
					sys.stdout.write("Num Offloads, ")
					print ', '.join(map(str, average_offload_num))
					sys.stdout.write("Std Dev Offload Time, ")
					print ', '.join(map(str, stdev_offload_time))
					print("\n")
				
				# initialize 2d matrix ~~~~~~~~~~~~~~~
				offload_times = []
				num_offloads = []
				for i in range(0, max_iterations) :
					offload_times.append([])
					num_offloads.append([])
					for j in range(0, max_kernels+1) :
						offload_times[i].append(0)
						num_offloads[i].append(0)
				#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

				#initialize stats matrices~~~~~~~~~~~~
				average_offload_time = []
				average_offload_num = []
				stdev_offload_time = []
				stdev_offload_num = []

				for i in range(0, max_kernels) :
					average_offload_time.append(0)
					average_offload_num.append(0)
					stdev_offload_time.append(0)
					stdev_offload_num.append(0)
				#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

			
		elif terms[2] == 'WGSIZE' :
			wgsize = terms[4]
			block_offload = 0
			thread_offload = 0
		elif terms[2] == 'INPUT' :
			data = terms[4]
		elif terms[2] == 'ITERATION' :
			iteration = terms[4]
			i = int(iteration)
		elif terms[2] == 'NUM_I' :
			num_iterations = int(terms[4])
		else :
			app = terms[2]
			kernel = terms[4]
			time = terms[6]
	
			offload_times[i-1][int(kernel[1])] = offload_times[i-1][int(kernel[1])] + int(time)
			num_offloads[i-1][int(kernel[1])] += 1;


	# Get Thread Data
	if len(readLine) >= 4 and (readLine[0] == 'T' and readLine[1] == 'H' and readLine[2] == 'R' and readLine[3] == 'E') :
		terms = readLine.split( )
		if terms[4] == 'COMPLETE' :
			thread_uptimes.pop(0)
			thread_downtimes.pop(0)
			num_threads.pop(0)

			thread_uptime_arr.pop(0)
			thread_downtime_arr.pop(0)

			efficiency = [0,0,0,0,0,0,0,0,0,0]

			uptime_avg = []
			uptime_stdev = []
			downtime_avg = []
			downtime_stdev = []
			
			for j in range(0, max_kernels) :
				if thread_uptimes[j] != 0 and thread_downtimes[j] != 0 :
					efficiency[j] = float(thread_uptimes[j]) / float(thread_uptimes[j] + thread_downtimes[j])
					
					uptime_avg.append(sum(thread_uptime_arr[j]) / len(thread_uptime_arr[j]))
					downtime_avg.append(sum(thread_downtime_arr[j]) / len(thread_downtime_arr[j]))
					uptime_stdev.append(numpy.std(thread_uptime_arr[j]))
					downtime_stdev.append(numpy.std(thread_downtime_arr[j]))
				else :
					uptime_avg.append(0)
					uptime_stdev.append(0)
					downtime_avg.append(0)
					downtime_stdev.append(0)
			
			if (thread_level == 1) and (thread_offload < offload_count) :
				print("\n~~THREAD DATA~~")
				print "Workgroup Size = " + wgsize
				print "Data = " + data
				print ', '.join(map(str, kernels))
				sys.stdout.write("Total Thread Uptime, ")
				print ', '.join(map(str, thread_uptimes))
				sys.stdout.write("Avg thread Uptime, ")
				print ', '.join(map(str, uptime_avg))
				sys.stdout.write("Stdev thread Uptime, ")
				print ', '.join(map(str, uptime_stdev))
				sys.stdout.write("Total Thread Downtime, ")
				print ', '.join(map(str, thread_downtimes))
				sys.stdout.write("Avg thread Downtime, ")
				print ', '.join(map(str, downtime_avg))
				sys.stdout.write("Stdev thread Downtime, ")
				print ', '.join(map(str, downtime_stdev))
				sys.stdout.write("Num Threads, ")
				print ', '.join(map(str, num_threads))
				sys.stdout.write("Efficiency, ")
				print ', '.join(map(str, efficiency))
				thread_offload += 1
			
			# re-initialize matrix ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			thread_uptimes = []
			thread_downtimes = []
			num_threads = []
			thread_uptime_arr = []
			thread_downtime_arr = []
			
			for i in range(0, max_kernels+1) :
				thread_uptimes.append(0)
				thread_downtimes.append(0)
				num_threads.append(0)
				thread_uptime_arr.append([])
				thread_downtime_arr.append([])

			thread_start = 0
			thread_end = 0
			#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			
		elif terms[2] == 'WGSIZE' :
			pass
		elif terms[2] == 'INPUT' :
			pass
		else :
			kernel = terms[2]
			if terms[4] == 'START' :
				thread_start = int(terms[6])
				if thread_end != 0 :
					if (thread_start - thread_end) < 0 :
						if thread_level == 1 :
							print "ERROR! Thread timestamp incorrect at line: " + str(line_number) + " (" + str(thread_start) + " - " + str(thread_end) + ")"
					else :
						thread_downtimes[int(kernel[1])] = thread_downtimes[int(kernel[1])] + (thread_start - thread_end)
						thread_downtime_arr[int(kernel[1])].append(thread_start - thread_end)
			if terms[4] == 'END' :
				thread_end = int(terms[6])
				if (thread_end - thread_start) < 0 :
					if thread_level == 1 :
						print "ERROR! Thread timestamp incorrect at line: " + str(line_number) + " (" + str(thread_end) + " - " + str(thread_start) + ")"
				else :
					thread_uptimes[int(kernel[1])] = thread_uptimes[int(kernel[1])] + (thread_end - thread_start)
					thread_uptime_arr[int(kernel[1])].append(thread_end - thread_start)
					num_threads[int(kernel[1])] += 1;






	# Get Workgroup Data
	if len(readLine) >= 4 and (readLine[0] == 'B' and readLine[1] == 'L' and readLine[2] == 'O' and readLine[3] == 'C') :
		terms = readLine.split( )
		if terms[4] == 'COMPLETE' :
			efficiency = []
			for i in range(0, 8) :
				efficiency.append([])
				for j in range(0, max_kernels+1) :
					efficiency[i].append(0)
			# efficiency[core_num][kernel]
			
			if core_select == -1 :
				core_start = 0
				core_end = 8
			else :
				core_start = core_select
				core_end = core_select+1

			for core in range(core_start, core_end) :
				block_uptimes[core].pop(0)
				block_downtimes[core].pop(0)
				num_blocks[core].pop(0)
				block_uptime_arr[core].pop(0)
				block_downtime_arr[core].pop(0)

				uptime_avg = []
				uptime_stdev = []
				downtime_avg = []
				downtime_stdev = []
		
				for j in range(0, max_kernels) :
					if block_uptimes[core][j] != 0 and block_downtimes[core][j] != 0 :
						efficiency[core][j] = float(block_uptimes[core][j]) / float(block_uptimes[core][j] + block_downtimes[core][j])
						uptime_avg.append(sum(block_uptime_arr[core][j]) / len(block_uptime_arr[core][j]))
						uptime_stdev.append(numpy.std(block_uptime_arr[core][j]))
						downtime_avg.append(sum(block_downtime_arr[core][j]) / len(block_downtime_arr[core][j]))
						downtime_stdev.append(numpy.std(block_downtime_arr[core][j]))
					else :
						uptime_avg.append(0)
						uptime_stdev.append(0)
						downtime_avg.append(0)
						downtime_stdev.append(0)
				
				if block_level == 1 and block_offload < offload_count :
					print("\n~~BLOCK DATA~~")
					print "Core num = " + str(core)
					print "Workgroup Size = " + wgsize
					print "Data = " + data
					print ', '.join(map(str, kernels))
					sys.stdout.write("Total block Uptime, ")
					print ', '.join(map(str, block_uptimes[core]))
					sys.stdout.write("Avg block Uptime, ")
					print ', '.join(map(str, uptime_avg))
					sys.stdout.write("Stdev block Uptime, ")
					print ', '.join(map(str, uptime_stdev))
					sys.stdout.write("Total block Downtime, ")
					print ', '.join(map(str, block_downtimes[core]))
					sys.stdout.write("Avg block Downtime, ")
					print ', '.join(map(str, downtime_avg))
					sys.stdout.write("Stdev block Downtime, ")
					print ', '.join(map(str, downtime_stdev))
					sys.stdout.write("Num blocks, ")
					print ', '.join(map(str, num_blocks[core]))
					sys.stdout.write("Efficiency, ")
					print ', '.join(map(str, efficiency[core]))
					block_offload += 1
			
			# re-initialize matrix and values ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			block_uptimes = []
			block_downtimes = []
			num_blocks = []
			block_start = []
			block_end = []
			block_start_temp = 0
			block_end_temp = 0

			block_uptime_arr = []
			block_downtime_arr = []
			
			for i in range(0, 8) :
				block_start.append(0)
				block_end.append(0)
	
				block_uptimes.append([])
				block_downtimes.append([])
				num_blocks.append([])

				block_uptime_arr.append([])
				block_downtime_arr.append([])

				for j in range(0, max_kernels+1) :
					block_uptimes[i].append(0)
					block_downtimes[i].append(0)
					num_blocks[i].append(0)
					block_uptime_arr[i].append([])
					block_downtime_arr[i].append([])

			#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			
		elif terms[2] == 'WGSIZE' :
			pass
		elif terms[2] == 'INPUT' :
			pass
		else :
			kernel = terms[2]
			core_num = int(terms[6])
			if core_num == -1 :
				print "ERROR! Block timestamp incorrect at line: " + str(line_number)
			block_start_temp = int(terms[8])
			block_end_temp = int(terms[10])
			
			if block_start[core_num] != 0 and block_end[core_num] != 0 :
				if (block_start_temp - block_end[core_num]) < 0 :
					if block_level == 1 :
						print "ERROR! Block timestamp incorrect at line: " + str(line_number) + " (" + str(block_start_temp) + " - " + str(block_end[core_num]) + ")"
				else :
					block_downtimes[core_num][int(kernel[1])] = int(block_downtimes[core_num][int(kernel[1])]) + (block_start_temp - block_end[core_num])
					block_downtime_arr[core_num][int(kernel[1])].append(block_start_temp - block_end[core_num])

			block_start[core_num] = block_start_temp
			block_end[core_num] = block_end_temp
			
			if (block_end[core_num] - block_start[core_num]) < 0 :
				if block_level == 1 :
					print "ERROR! Block timestamp incorrect at line: " + str(line_number) + " (" + str(block_end[core_num]) + " - " + str(block_start[core_num]) + ")"
			else :
				block_uptimes[core_num][int(kernel[1])] = block_uptimes[core_num][int(kernel[1])] + (block_end[core_num] - block_start[core_num])
				block_uptime_arr[core_num][int(kernel[1])].append(block_end[core_num] - block_start[core_num])
				num_blocks[core_num][int(kernel[1])] += 1
















